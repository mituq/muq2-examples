\section{Problem statement}

Our goal is to infer soil and rock properties given observations of a contaminate flowing through an aquifer.  We consider an unconfined aquifer, an underground water resource where the water table is not restricted by geographic features (e.g., bedrock).  Soil and rock properties determine the spatially dependent transmissivity field $\kappa(x)$.  The transmissivity determines how fluids flow through the aquifer.

Consider a square domain that is divided into six irregular regions (figure \ref{fig:domain}).  Each region corresponds to a geological feature or region (e.g., bedrock, clay, or gravel) with a different transmissivity.  We assume that we know the boundaries of these regions but not the transmissivity itself.  The top and bottom boundaries have no-flow conditions, either due to topoglogical (e.g., rigids) or geological (e.g., bedrock outcropings) features.

In general, the transmisivity in each region is uncertain.  We will infer these values given observations of a non-reactive tracer.  We assume that sensors are placed on a $4 \times 4$ grid over the domain.  The black dots in figure \ref{fig:domain} represent these observation locations.  In addition to the sensors, we assume that there is a well in the domain at $w = [0.15, 0.75]$.  This is shown as the white diamond in figure \ref{fig:domain}.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1.0\textwidth]{\figures fig_Domain.png}
  \end{subfigure}
  \caption{The unconfined aquifer is divided into $6$ regions.  The top and bottom boundaries have no-flow conditions either due to topological or geological features and there is a river on the right side of the aquifer.  Each of the $6$ regions is assigned a constant transmissivity $\kappa_i$.  The black dots represent locations where we can observe the tracer concentration.  The white diamond at $w = [0.15, 0.75]$ represents a well.}
  \label{fig:domain}
\end{figure}

\subsection{Hydraulic head and Darcy velocity}

The flow in the aquifer is determined by the hydralic head and corresponding Darcy velocities.  The steady state equation used to compute the hydraulic head $h$ is\footnote{Given test function $\phi$ such that $\phi = 0$ on $\partial \Omega_d$, the weak form is $$\int \nabla \phi \cdot \left(\kappa h \nabla h \right) - \phi f_h dx = 0.$$}

\begin{subequations}
\begin{equation}
  \nabla \cdot \left(\kappa h \nabla h \right) = -f_h
  \label{eq:hydraulic-head-eq}
\end{equation}

\noindent with boundary conditions

\begin{equation}
  \begin{array}{ccc}
    \nabla h \cdot n = 0 & \mbox{when} & x \in \partial \Omega_n \\
    h = h_0 & \mbox{when} & x \in \partial \Omega_d,
  \end{array}
  \label{eq:hydraulic-head-bc}
\end{equation}

\noindent where $\partial \Omega_n$ are the no-flow boundaries along the top and bottom of the domain and $\partial \Omega_d$ are the right and left boundaries, where we assume the hydralic head is known.  Let $\partial \Omega = \partial \Omega_d \cup \partial \Omega_n$ be the entire boundary.  We assume that the well is pumping water out of the aquifer and, therefore, creates a sink for the hydralic head

\begin{equation}
  f_h(x) = -10 \exp{\left( \left( (x_1-w_1)^2+(x_2-w_2)^2 \right)/\num{5e-3}\right)}.
  \label{eq:hydraulic-head-forcing}
\end{equation}

\noindent We also assume the head at the left and right boundaries is known

\begin{equation}
  h_0(x) = \begin{cases}
  0.5 x_2 + 1.5 & \mbox{if } x_1 = 1 \\
  0.5 x_2 + 2.5 & \mbox{if } x_1 = 0.
  \end{cases}
  \label{eq:hydraulic-head-Dirichlet}
\end{equation}
\label{eq:hydraulic-head}
\end{subequations}

\noindent The Darcy velocity is

\begin{equation}
  u = - \kappa h \nabla h.
  \label{eq:velocity}
\end{equation}

\noindent  Given the transmisivity $\kappa$, we compute the steady-state hydraulic head and Darcy velocities using \eqref{eq:hydraulic-head}.  In general, $\kappa$ is unknown/uncertain.  However, we prescribe ``true'' values

\begin{equation}
  \begin{array}{ccccccc}
    \log{\kappa_1} = 0, & \log{\kappa_2} = -5, & \log{\kappa_3} = -1, & \log{\kappa_4} = -2, & \log{\kappa_5} = -1.5, & \mbox{and} & \log{\kappa_6} = -3,
  \end{array}
  \label{eq:true-transmisivity}
\end{equation}

\noindent corresponding to each of the regions in figure \ref{fig:domain}.  The solution to \eqref{eq:hydraulic-head} given \eqref{eq:true-transmisivity} is shown in figure \ref{fig:hydraulic-head}.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[b]{0.75\textwidth}
    \includegraphics[width=1.0\textwidth]{\figures fig_HydraulicHead.png}
  \end{subfigure}
  \caption{The colormap represents hydraulic head, which is the solution to \eqref{eq:hydraulic-head}, given the ``true'' transmisivity \eqref{eq:true-transmisivity}. The arrows are the Darcy velocities computed via \eqref{eq:velocity}.}
  \label{fig:hydraulic-head}
\end{figure}

\subsection{Tracer transport}

Given the steady-state Darcy velocities \eqref{eq:velocity}, a non-reactive tracer is transported through the domain by the advection-diffusion equation\footnote{Given test function $\phi$, the weak form is $$\left. -\phi c (u \cdot n) \right| - \left. \phi r c \right|_{\partial \Omega_r} + \int \phi \frac{\partial c}{\partial t} - \nabla \phi \cdot \left(\left( d_m I + d_l u u^T  \right) \nabla c\right) + c \left(u \cdot \nabla \phi\right) + \phi f_t dx =0$$}

\begin{subequations}
\begin{equation}
  \frac{\partial c}{\partial t} + \nabla \cdot \left(\left( d_m I + d_l u u^T  \right) \nabla c\right) - u \cdot \nabla c = -f_t,
  \label{eq:concentration-eq}
\end{equation}

\noindent where $d_m, d_l \in \mathbb{R}$, $I$ is the identity matrix, and $f_t$ is a models sources/sinks of the tracer; we assume $d_l=\num{e-2}$ and $d_m=\num{e-2}$.  We impose boundary conditions

\begin{equation}
  \begin{array}{ccc}
    \left( d_m I + d_l u u^T  \right) \nabla c \cdot n = -r (c-c_b) & \mbox{when} & x \in \partial \Omega,
  \end{array}
\end{equation}

\noindent where $r$ models how quickly the tracer diffuses out of the domain and $c_b$ is the concentration on the other side of the domain boundary.  We assume that

\begin{equation}
  c_b(x) = \begin{cases}
  0 & \mbox{if } x_1=1 \mbox{ (aquifer-river interface)} \\
  c & \mbox{if } x_1=0,\, x_2=0, \mbox{ or } x_2=1. \\
  \end{cases}
\end{equation}

\noindent This simplifies the boundary condition to

\begin{equation}
  \begin{array}{ccc}
    \left( d_m I + d_l u u^T  \right) \nabla c \cdot n = -r c & \mbox{when} & x \in \partial \Omega_r \\
    \left( d_m I + d_l u u^T  \right) \nabla c \cdot n = 0 & \mbox{when} & x \in \partial \Omega_r \setminus \partial \Omega_r,
  \end{array}
  \label{eq:concentration-bc}
\end{equation}

\noindent where $\Omega_r$ is the aquifer-river interface.  We assume $r=0$.

We model an initial spill at the well location of the tracer by prescribing initial conditions

\begin{equation}
  c(x, t=0) = \exp{\left( \left( (x_1-w_1)^2+(x_2-w_2)^2 \right)/\num{5e-3}\right)},
  \label{eq:concentration-initial}
\end{equation}

\noindent shown in figure \ref{fig:concentration-initial-condition}.  We also model continued leakage at the spill site by prescribing

\begin{equation}
  f_t(x) = 10 \exp{\left( \left( (x_1-w_1)^2+(x_2-w_2)^2 \right)/\num{5e-3}\right)}.
  \label{eq:concentration-forcing}
\end{equation}
\label{eq:concentration}
\end{subequations}

\noindent The solution to the tracer-transport equation \eqref{eq:concentration} is shown in figure \ref{fig:concentration}.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[b]{0.475\textwidth}
    \includegraphics[width=1.0\textwidth]{\figures fig_ConcentrationStep0.png}
    \caption{$t=0$}
    \label{fig:concentration-initial-condition}
  \end{subfigure}
  \begin{subfigure}[b]{0.475\textwidth}
    \includegraphics[width=1.0\textwidth]{\figures fig_ConcentrationStep1.png}
    \caption{$t=0.2$}
  \end{subfigure}
  \begin{subfigure}[b]{0.475\textwidth}
    \includegraphics[width=1.0\textwidth]{\figures fig_ConcentrationStep2.png}
    \caption{$t=0.4$}
  \end{subfigure}
  \begin{subfigure}[b]{0.475\textwidth}
    \includegraphics[width=1.0\textwidth]{\figures fig_ConcentrationStep3.png}
    \caption{$t=0.6$}
  \end{subfigure}
  \begin{subfigure}[b]{0.475\textwidth}
    \includegraphics[width=1.0\textwidth]{\figures fig_ConcentrationStep4.png}
    \caption{$t=0.8$}
  \end{subfigure}
  \begin{subfigure}[b]{0.475\textwidth}
    \includegraphics[width=1.0\textwidth]{\figures fig_ConcentrationStep5.png}
    \caption{$t=1$}
  \end{subfigure}
  \caption{Tracer concentration computed for times $t \in \{0, 0.2, 0.4, 0.6, 0.8, 1\}$ by solving \eqref{eq:concentration-eq} with boundary conditions \eqref{eq:concentration-bc} and initial conditions \eqref{eq:concentration-initial}.  The tracer continues to leak into the domain according to $f_t$ prescribed by \eqref{eq:concentration-forcing}.  We assume $d_l=\num{e-2}$, $d_m=\num{e-2}$, and $r=0$.  Grey dots correspond to sensor locations.}
  \label{fig:concentration}
\end{figure}
