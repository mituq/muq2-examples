import sys

# import numpy
import numpy as np

# import dolfin/fenics
import dolfin as dfn
#dfn.set_log_level(50) # turn off printing

# import muq Modeling
import pymuqModeling as mm

# import matplotlib so that we can make figures
import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib.colors import LinearSegmentedColormap

import random

folder = 'figures' # save figures here
data = 'data' # save output data here

xwell = 0.2
ywell = 0.85

def SetrcParams(facecolor, plotcolor):
    rcParams['font.family'] = 'serif'
    rcParams['text.usetex'] = True
    rcParams['axes.edgecolor'] = plotcolor
    rcParams['axes.facecolor'] = facecolor
    rcParams['figure.facecolor'] = facecolor
    rcParams['axes.labelcolor'] = plotcolor
    rcParams['text.color'] = plotcolor
    rcParams['xtick.color'] = plotcolor
    rcParams['ytick.color'] = plotcolor

def CreateDomain(nx, ny, datax, datay, comm):
    mesh = dfn.RectangleMesh(comm, dfn.Point(0.0, 0.0), dfn.Point(1.0, 1.0), nx, ny)

    # refine top portion
    for i in range(2):
        cell_markers = dfn.MeshFunction("bool", mesh, 2)
        cell_markers.set_all(False)
        for cell in dfn.cells(mesh):
            p = cell.midpoint()
            if p.x()>0.1 and (p.x()<0.5 or i==0) and p.y()>0.5 and p.y()<0.95:
                cell_markers[cell] = True
        mesh = dfn.refine(mesh, cell_markers)

    # create class for defining the 6 regions
    class Region(dfn.SubDomain):
        def __init__(self, num):
            dfn.SubDomain.__init__(self)
            # the number of this domain
            self.num = num

            # coordinates to keep track of the domain
            self.x = [[0.6, 1.0], [0.3, 0.45], [0.0, 0.4], [0.5, 1.0], [0.4, 1.0], [0.0, 1.0]]
            self.y = [[0.7, 0.85], [0.1, 0.9], [0.7, 1.0], [0.0, 0.5], [0.6, 1.0], [0.0, 1.0]]

        def inside(self, p, on_boundary):
            for i in range(6):
                if p[0]>self.x[i][0] and p[0]<self.x[i][1] and p[1]>self.y[i][0] and p[1]<self.y[i][1]:
                    return self.num==i
            return self.num==6

    # initialize the regins
    regions = [Region(i) for i in range(6)]

    # create and mark the domains
    domains = dfn.MeshFunction("size_t", mesh, mesh.topology().dim())
    for i in range(6):
        regions[i].mark(domains, i)

    if comm.Get_size()==1:
        # plot the mesh
        plt.figure(figsize=(15,5))
        SetrcParams('#ffffff', '#969696')
        ax = plt.subplot(121)
        dfn.plot(mesh)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.0])
        ax.set_ylabel(r'$y$', fontsize=16)
        ax.set_xlabel(r'$x$', fontsize=16)
        ax = plt.subplot(122)
        dfn.plot(domains)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.0])
        ax.set_ylabel(r'$y$', fontsize=16)
        ax.set_xlabel(r'$x$', fontsize=16)
        plt.savefig(folder+'/fig_Mesh.png', format='png', bbox_inches='tight')

        base = plt.cm.jet
        color_list = [
        (120.0/255.0,120.0/255.0,120.0/255.0, 1.0),
        (166.0/255.0,86.0/255.0,40.0/255.0, 1.0),
        (77.0/255.0,175.0/255.0,74.0/255.0, 1.0),
        (152.0/255.0,78.0/255.0,163.0/255.0, 1.0),
        (255.0/255.0,127.0/255.0,0.0/255.0, 1.0),
        (200.0/255.0,26.0/255.0,28.0/255.0, 1.0)
        ]
        cmap_name = base.name + str(6)
        cm = base.from_list(cmap_name, color_list, 6)

        X, Y = np.meshgrid(datax, datay)

        plt.figure(figsize=(15,5))
        SetrcParams('#ffffff', '#969696')
        ax = plt.gca()
        dfn.plot(domains, cmap=cm)
        ax.fill_between([1.0, 1.2], -0.1, 1.1, color='#377eb8')
        ax.plot(X, Y, 'o', markeredgecolor='#252525', markerfacecolor='#252525', markersize=5)
        ax.plot(xwell, ywell, 'd', markeredgecolor='#cccccc', markerfacecolor='#cccccc', markersize=10)
        ax.text(0.8, 0.75, '1', color='#1a1a1a', fontsize=22)
        ax.text(0.365, 0.5, '2', color='#1a1a1a', fontsize=22)
        ax.text(0.1, 0.825, '3', color='#1a1a1a', fontsize=22)
        ax.text(0.75, 0.2, '4', color='#1a1a1a', fontsize=22)
        ax.text(0.8, 0.9, '5', color='#1a1a1a', fontsize=22)
        ax.text(0.15, 0.25, '6', color='#1a1a1a', fontsize=22)
        ax.text(0.25, 1.05, 'Unconfined aquifer', color='#1a1a1a', fontsize=16)
        ax.text(1.02, 0.75, 'River', color='#1a1a1a', fontsize=16)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.set_xlim([0.0, 1.2])
        ax.set_ylim([-0.1, 1.1])
        ax.set_ylabel(r'$y$', fontsize=16)
        ax.set_xlabel(r'$x$', fontsize=16)
        plt.savefig(folder+'/fig_Domain.png', format='png', bbox_inches='tight')

    return mesh, regions

class HydraulicHeadForcing(mm.PyModPiece):
    def __init__(self, mesh):
        """
        Args:
            paam1 (mesh): The domain mesh
        """
        # to help with ploting later
        self.mesh = mesh

        # create a function space for the forcing parameter
        Vp = dfn.FunctionSpace(mesh, 'Lagrange', 1)

        dm = Vp.dofmap()
        owned = dm.ownership_range()
        dofs_on_proc = owned[1] - owned[0]

        # initalize the ModPiece
        mm.PyModPiece.__init__(self, [], [dofs_on_proc])

        # create the fenics function
        self.fh = dfn.Function(Vp)

        # define the hydraulic forcing at a point
        class HHForcing(dfn.Expression):
            def eval(self, values, x):
                diffx = x[0]-xwell
                diffy = x[1]-ywell
                values[0] = -10.0*np.exp(-(diffx*diffx+diffy*diffy)/0.005)

        # evaluate at the DOFs
        expression = HHForcing(element=Vp.ufl_element())
        self.fh.interpolate(expression)

    def EvaluateImpl(self, inputs):
        # gather the vector
        self.outputs = [self.fh.vector().get_local()]

    def Function(self):
        return self.fh

    def PlotField(self):
        file = dfn.File(data+'/HydraulicHeadForcing.pvd')
        file << self.fh

        if self.mesh.mpi_comm().Get_size()==1:
            fig = plt.figure(figsize=(15,5))
            SetrcParams('#ffffff', '#969696')
            ax = plt.gca()
            s = dfn.plot(self.fh)
            fig.colorbar(s)
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.set_xlim([0.0, 1.0])
            ax.set_ylim([0.0, 1.0])
            ax.set_ylabel(r'$y$', fontsize=16)
            ax.set_xlabel(r'$x$', fontsize=16)
            plt.savefig(folder+'/fig_HydraulicHeadForcing.png', format='png', bbox_inches='tight')

class LogTransmissivity(mm.PyModPiece):
    def __init__(self, mesh, regions, Vp):
        """
        Args:
            param1 (mesh): The domain mesh
            param2 (regions): The regions where the transmissivity is constant
        """
        self.cf = dfn.CellFunction('size_t', mesh, 0)
        for i in range(len(regions)):
            regions[i].mark(self.cf, i)

        # store the function space for the parameter log-kappa
        self.Vp = Vp

        dm = self.Vp.dofmap()
        owned = dm.ownership_range()
        dofs_on_proc = owned[1] - owned[0]

        mm.PyModPiece.__init__(self, [6], [dofs_on_proc])

    def EvaluateImpl(self, inputs):
        # create the function
        logk = dfn.Function(self.Vp)

        # set the regions
        cf = self.cf

        # create the expression
        class LogTrans(dfn.Expression):
            def eval_cell(self, values, x, ufc_cell):
                for i in range(6):
                    if cf.array()[ufc_cell.index]==i:
                        values[0] = inputs[0][i]
                        return
        # interpolate expression to function
        expression = LogTrans(element=self.Vp.ufl_element())
        logk.interpolate(expression)

        # set the outputs
        self.outputs = [logk.vector().get_local()]

    def SaveField(self, logk):
        logKappa = dfn.Function(self.Vp)
        logKappa.vector().set_local(logk)
        logKappa.vector().apply('')

        file = dfn.File(data+'/LogTransmissivity.pvd')
        file << logKappa

class TracerForcing(mm.PyModPiece):
    def __init__(self, mesh):
        """
        Args:
            paam1 (mesh): The domain mesh
        """
        self.mesh = mesh # to help with plotting

        # create a function space for the forcing parameter
        Vp = dfn.FunctionSpace(mesh, 'Lagrange', 1)

        dm = Vp.dofmap()
        owned = dm.ownership_range()
        dofs_on_proc = owned[1] - owned[0]

        # initalize the ModPiece
        mm.PyModPiece.__init__(self, [], [dofs_on_proc])

        # create the fenics function
        self.ft = dfn.Function(Vp)

        # define the hydraulic forcing at a point
        class TForcing(dfn.Expression):
            def eval(self, values, x):
                diffx = x[0]-xwell
                diffy = x[1]-ywell
                values[0] = 10.0*np.exp(-(diffx*diffx+diffy*diffy)/0.005)

        # evaluate at the DOFs
        expression = TForcing(element=Vp.ufl_element())
        self.ft.interpolate(expression)

    def EvaluateImpl(self, inputs):
        # store the vector
        self.outputs = [self.ft.vector().get_local()]

    def Function(self):
        return self.ft

    def PlotField(self):
        file = dfn.File(data+'/TracerForcing.pvd')
        file << self.ft

        if self.mesh.mpi_comm().Get_size()==1:
            fig = plt.figure(figsize=(15,5))
            SetrcParams('#ffffff', '#969696')
            ax = plt.gca()
            s = dfn.plot(self.ft)
            fig.colorbar(s)
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.set_xlim([0.0, 1.0])
            ax.set_ylim([0.0, 1.0])
            ax.set_ylabel(r'$y$', fontsize=16)
            ax.set_xlabel(r'$x$', fontsize=16)
            plt.savefig(folder+'/fig_TracerForcing.png', format='png', bbox_inches='tight')

def InitialConditions(Vc):
    class InitialConcentration(dfn.Expression):
        def eval(self, values, x):
            diffx = x[0]-xwell
            diffy = x[1]-ywell
            values[0] = np.exp(-(diffx*diffx+diffy*diffy)/0.005)
    expression = InitialConcentration(element=Vc.ufl_element())
    cinit = dfn.Function(Vc)
    cinit.interpolate(expression)
    return cinit

def PlotConcentration(c, Vc, step, draw):
    concentration = dfn.Function(Vc)
    concentration.vector().set_local(c)
    concentration.vector().apply('')

    file = dfn.File(data+'/ConcentrationStep'+str(step)+'.pvd')
    file << concentration

    if draw:
        datax = np.linspace(0.05, 0.95, num=4)
        datay = np.linspace(0.05, 0.95, num=4)

        X, Y = np.meshgrid(datax, datay)

        fig = plt.figure(figsize=(15,5))
        SetrcParams('#ffffff', '#969696')
        ax = plt.gca()
        s = dfn.plot(concentration, cmap='jet', extend='max', vmin=0.0)
        ax.plot(X, Y, 'o', markeredgecolor='#636363', markerfacecolor='#636363',    markersize=5)
        cbar = fig.colorbar(s)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.0])
        ax.set_ylabel(r'$y$', fontsize=16)
        ax.set_xlabel(r'$x$', fontsize=16)
        plt.savefig(folder+'/fig_ConcentrationStep'+str(step)+'.png', format='png', bbox_inches='tight')

def River(x, on_boundary):
    return on_boundary and abs(1.0-x[0])<dfn.DOLFIN_EPS

def Hill(x, on_boundary):
    return on_boundary and abs(x[0])<dfn.DOLFIN_EPS

def Top(x, on_boundary):
    return on_boundary and abs(1.0-x[1])<dfn.DOLFIN_EPS

def Bottom(x, on_boundary):
    return on_boundary and abs(x[1])<dfn.DOLFIN_EPS

class HydraulicHead(mm.PDEModPiece):
    def __init__(self, fh, mesh, Vp, Vh):
        # to help plot later
        self.Vh = Vh
        self.Vp = Vp
        self.mesh = mesh

        self.fh = fh
        self.eps = 0.0

        # define the boundary condition for the forward problem
        h0 = dfn.Constant(1.0)
        class RiverHead(dfn.Expression):
            def eval(self, values, x):
                values[0] = 0.5*x[1]+1.5
        h0 = RiverHead(element=Vh.ufl_element())
        bc_fwd1 = dfn.DirichletBC(Vh, h0, River)
        class HillHead(dfn.Expression):
            def eval(self, values, x):
                values[0] = 0.5*x[1]+2.5
        h1 = HillHead(element=Vh.ufl_element())
        bc_fwd2 = dfn.DirichletBC(Vh, h1, Hill)

        # define the boundary condition for the adjoint problem
        p0 = dfn.Constant(1.0)
        bc_adj1 = dfn.DirichletBC(Vh, p0, River)
        bc_adj2 = dfn.DirichletBC(Vh, p0, Hill)

        # create the PDE model
        mm.PDEModPiece.__init__(self, Vh, Vp, self.weak_form, [bc_fwd1, bc_fwd2], [bc_adj1, bc_adj2], is_fwd_linear=False)

    def InitialGuess(self):
        if hasattr(self, 'init'):
            return self.init

        self.init = self.pde.generate_state()

        self.eps = 1.0
        while self.eps>0.05:
            # solve the forward model
            self.pde.solveFwd(self.init, [self.init, self.para, None], 1.0e-4)
            self.eps = self.eps/5.0
        self.eps = 0.0

        return self.init

    def weak_form(self, h, k, p):
        return dfn.inner((self.eps+dfn.exp(k)*h)*dfn.grad(h), dfn.grad(p))*dfn.dx - self.fh*p*dfn.dx

    def PlotField(self, h, logk):
        head = dfn.Function(self.Vh)
        head.vector().set_local(h)
        head.vector().apply('')

        file = dfn.File(data+'/HydraulicHead.pvd')
        file << head

        if self.mesh.mpi_comm().Get_size()==1:
            logkap = dfn.Function(self.Vp)
            logkap.vector().set_local(logk)
            vel = dfn.project(-head*dfn.exp(logkap)*dfn.grad(head))

            x = np.linspace(0.025, 1.0, num=25)
            y = np.linspace(0.0, 1.0, num=25)
            X, Y = np.meshgrid(x, y)

            U = np.array([[0.0]*len(x)]*len(y))
            V = np.array([[0.0]*len(x)]*len(y))

            for i in range(len(x)):
                for j in range(len(y)):
                    U[j,i] = vel([x[i], y[j]]) [0]
                    V[j,i] = vel([x[i], y[j]]) [1]

            fig = plt.figure(figsize=(15,5))
            SetrcParams('#ffffff', '#969696')
            ax = plt.gca()
            s = dfn.plot(head, cmap='jet')
            ax.quiver(X, Y, U, V, scale=8.0, width=0.005, color='#969696')
            #fig.colorbar(s, ticks=linspace(min(h), max(h), num=5)))
            fig.colorbar(s)
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.set_xlim([0.0, 1.0])
            ax.set_ylim([0.0, 1.0])
            ax.set_ylabel(r'$y$', fontsize=16)
            ax.set_xlabel(r'$x$', fontsize=16)
            plt.savefig(folder+'/fig_HydraulicHead.png', format='png', bbox_inches='tight')

class TracerTransportRHS(mm.PyModPiece):
    def __init__(self, Vp, Vh, Vc, mesh, ft):
        self.Vc = Vc
        # test and trial functions
        self.dcdt = dfn.TrialFunction(Vc)
        self.w = dfn.TestFunction(Vc)

        # bilinear form
        self.a = self.w*self.dcdt*dfn.dx
        self.M = dfn.assemble(self.a) # mass matrix
        self.solver = dfn.LUSolver(self.M)
        #self.solver.parameters['reuse_factorization'] = True

        self.c = dfn.Function(Vc) # previous conditions
        self.logk = dfn.Function(Vp) # log-transmissivity
        self.h = dfn.Function(Vh) # hydraulic head
        self.ft = ft # forcing function
        #self.dm = dfn.Constant(1.0)

        # linear form
        #self.L = self.w*self.ft*dfn.dx \
        #    - dm*dfn.inner(dfn.grad(self.w), dfn.grad(self.c))*dfn.dx \
        #    - dl*dfn.inner(dfn.grad(self.w), dfn.outer(u, u)*dfn.grad(self.c))*dfn.dx \
        #    + self.c*dfn.inner(u, dfn.grad(self.w))*dfn.dx \
        #    - self.w*self.c*dfn.inner(u, self.normal)*self.ds(1)
        self.b = None # linear form vector

        # the solution (dcdt)
        self.soln = dfn.Function(Vc)

        class RiverDomain(dfn.SubDomain):
            def inside(self, x, on_boundary):
                return River(x, on_boundary)
        class HillDomain(dfn.SubDomain):
            def inside(self, x, on_boundary):
                return Hill(x, on_boundary)
        self.boundaries = dfn.FacetFunction("size_t", mesh, 0)
        RiverDomain().mark(self.boundaries, 1)
        HillDomain().mark(self.boundaries, 2)

        # store the river boundary
        self.ds = dfn.ds(domain=mesh, subdomain_data=self.boundaries)

        # store the normals
        self.normal = dfn.FacetNormal(mesh)

        # local concentration size
        cSize = self.FunctionSpaceSize(Vc)

        # local parameter size
        pSize = self.FunctionSpaceSize(Vp)

        # local hydraulic head size
        hSize = self.FunctionSpaceSize(Vh)

        self.comm = mesh.mpi_comm()

        mm.PyModPiece.__init__(self, [cSize, pSize, hSize, 2, 1], [cSize])

    def FunctionSpaceSize(self, V):
        dm = V.dofmap()
        owned = dm.ownership_range()
        return owned[1] - owned[0]

    def EvaluateImpl(self, inputs):
        #self.comm.Barrier()

        #print('hello from rank:', self.comm.Get_rank())
        #sys.stdout.flush()

        # previous conditions
        self.c.vector().set_local(inputs[0])
        self.c.vector().apply('')

        #print('hello from rank:', self.comm.Get_rank(), 'total concentration: ', dfn.assemble(self.c*dfn.dx))
        #sys.stdout.flush()

        # log-transmissivity
        self.logk.vector().set_local(inputs[1])
        self.logk.vector().apply('')

        # hydraulic head
        self.h.vector().set_local(inputs[2])
        self.h.vector().apply('')

        # # diffusion constants
        dm = dfn.Constant(inputs[3][0])
        dl = dfn.Constant(inputs[3][1])

        # hill and river boundary parameter
        r = dfn.Constant(inputs[4][0])

        # velocity
        u = -self.h*dfn.exp(self.logk)*dfn.grad(self.h)

        self.dcdt = dfn.TrialFunction(self.Vc)
        self.w = dfn.TestFunction(self.Vc)

        # bilinear form
        #a = self.w*self.dcdt*dfn.dx

        # linear form
        L = self.w*self.ft*dfn.dx \
            - dm*dfn.inner(dfn.grad(self.w), dfn.grad(self.c))*dfn.dx \
            - dl*dfn.inner(dfn.grad(self.w), dfn.outer(u, u)*dfn.grad(self.c))*dfn.dx \
            + self.c*dfn.inner(u, dfn.grad(self.w))*dfn.dx \
            - self.w*self.c*dfn.inner(u, self.normal)*self.ds(1) \
            + self.w*self.c*dfn.inner(u, self.normal)*self.ds(2) \
            - r*self.w*self.c*self.ds(1)
        self.b = dfn.assemble(L, tensor=self.b)

        # solve the system
        #dfn.solve(self.M, self.soln.vector(), self.b)
        self.solver.solve(self.soln.vector(), self.b)
        #dfn.solve(a==L, self.soln)

        #print('hello from rank:', self.comm.Get_rank(), 'output: ', self.soln.vector().get_local())

        self.outputs = [self.soln.vector().get_local()]
