import sys
sys.path.append('../') # dir. where the forward model is implemented

# import the forward model classes
from ForwardModel import *

# import ParCer
import pyParCer as parcer

# import muq Modeling
import pymuqModeling as mm

from time import time

# import dolfin/fenics
import dolfin as dfn
dfn.set_log_level(50) # turn off printing

from time import time

comm = dfn.mpi_comm_world()

nx = 40 # number of elements in the x direction
ny = 40 # number of elements in the y direction

# locations where we have observations
datax = np.linspace(0.05, 0.95, num=4)
datay = np.linspace(0.05, 0.95, num=4)

# create the domain (mesh and regions)
mesh, regions = CreateDomain(nx, ny, datax, datay, comm)

# create a function space for the parameter log-kappa
Vp = dfn.FunctionSpace(mesh, 'Lagrange', 1)

# create a function space for the hydaulic head unknown
Vh = dfn.FunctionSpace(mesh, 'Lagrange', 1)

# create a function space for the tracer unknown
Vc = dfn.FunctionSpace(mesh, 'Lagrange', 1)

# create the true log-transmissivity field
trueLogKappa = [0.0, -5.0, -1.0, -2.5, -1.5, -3.0]
logKappa = LogTransmissivity(mesh, regions, Vp)

# evaluate and plot the transmissivity
logk = logKappa.Evaluate([trueLogKappa]) [0]
logKappa.SaveField(logk)

# create the forcing for the steady state hydraulic head equation
hydraulicF = HydraulicHeadForcing(mesh)
fh = hydraulicF.Function()
hydraulicF.PlotField()

# compute the hydraulic head
head = HydraulicHead(fh, mesh, Vp, Vh)
if comm.Get_rank()==0:
    print('computing the hydraulic head...')
sys.stdout.flush()
t = time()
h = head.Evaluate([logk]) [0]
if comm.Get_rank()==0:
    print('\tfinished in', time()-t, 'seconds')
sys.stdout.flush()
head.PlotField(h, logk)

tracerF = TracerForcing(mesh)
ft = tracerF.Function()
tracerF.PlotField()

dm = 1.0e-2 # diffusion
dl = 1.0e-2 # velocity diffusion
r = 0.0 # rate of diffusion into the river

outTimes = np.linspace(0.0, 1.0, num=6)

# initial concentration
cinit = InitialConditions(Vc)

# tracer transport rhs
rhs = TracerTransportRHS(Vp, Vh, Vc, mesh, ft)

pt = dict()
pt['RelativeTolerance'] = 1.0e-3
pt['AbsoluteTolerance'] = 1.0e-3
pt['MaxStepSize'] = 100.0
pt['NumObservations'] = len(outTimes)
pt['MultistepMethod'] = 'Adams'
pt['NonlinearSolver'] = 'Newton'
pt['LinearSolver'] = 'SPGMR'
pt['MaxNumSteps'] = int(1e4)
pt['GlobalSize'] = Vc.dim()

# the time stepper
ode = mm.ODE(rhs, pt, parcer.Communicator(comm.tompi4py()))

start = time()
result = ode.Evaluate([cinit.vector().get_local(), logk, h, [dm, dl], [r], outTimes]) [0]
end = time()
print('time for time integration:', end-start)

dm = Vc.dofmap()
owned = dm.ownership_range()
size = owned[1] - owned[0]
for i in range(len(outTimes)):
    PlotConcentration(result[i*size:(i+1)*size], Vc, i, comm.Get_size()==1)
