from dolfin import *
set_log_level(50)
import numpy as np
#import h5py

# MUQ Includes
import pymuqModeling as mm

class BuoyModel(mm.PyModPiece):
    
    def __init__(self, xobs, D):
        mm.PyModPiece.__init__(self, [3], [len(xobs)])

        # Parameters
        self.D = D # Domain length
        self.num = 50 # Number of elements
        self.temp_water = 1.0 + 273 # Water temperature (far field)
        self.tau = 1.0 # Robin BC constant
        self.T_air = -20.0 + 273.0 # Air temperature BC

        self.xobs = xobs

    def EvaluateImpl(self, inputs):
        k_i = np.exp(inputs[0][0])
        k_w = np.exp(inputs[0][1])
        h = np.exp(inputs[0][2])/self.D

        # Create mesh and define function space
        mesh = UnitIntervalMesh(self.num)
        V = FunctionSpace(mesh, "Lagrange", 2)

        # Create class for defining the ice domain
        class Ice(SubDomain):        
            def inside(self, x, on_boundary):
                return between(x[0], (1.0 - h, 1.0))

        # Initialize ice sub-domain instance
        ice = Ice()

        # Initialize mesh function for interior domains
        domains = MeshFunction("size_t", mesh, mesh.topology().dim())
        domains.set_all(0)
        ice.mark(domains, 1)

        # Define Dirichlet boundary (x = 0)
        def boundary(x):
            return x[0] < DOLFIN_EPS

        # Define boundary condition
        u0 = Constant(self.temp_water)
        bc = DirichletBC(V, u0, boundary)

        # Define new measures associated with the domains
        dx = Measure('dx', domain=mesh, subdomain_data=domains)

        # Define variational problem
        u = TrialFunction(V)
        v = TestFunction(V)

        # Weak form
        a = -Constant(k_w)*inner(grad(u), grad(v))*dx(0) - \
            Constant(k_i)*inner(grad(u), grad(v))*dx(1) - Constant(self.tau)*u*v*ds
        L = Constant(0.0)*v*(dx(0) + dx(1)) - Constant(self.tau)*Constant(self.T_air)*v*ds

        # Compute solution
        T = Function(V)
        solve(a == L, T, bc)

        self.outputs = [np.array([T(x/self.D) for x in self.xobs])]
