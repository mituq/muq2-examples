import pymuqModeling as mm
import pymuqApproximation as ma
import numpy as np

import matplotlib.pyplot as plt

def HeatFlux(x):
    """ Returns the right hand side of the heat equation at a point x. """
    return 10.0

def GetPriorGP():
    var = 4.0
    length = 0.2
    kern = ma.SquaredExpKernel(spaceDim, var, length)

    mu = ma.ZeroMean(spaceDim,1)

    return ma.GaussianProcess(mu,kern)

def SolveFEM(obsPts, logK, numNodes):
    import scipy.sparse as sp
    import scipy.sparse.linalg as spla

    numCells = numNodes-1
    dx = 1.0/numCells
    pts = np.linspace(0,1,numNodes)

    mainDiag = 2.0*np.ones(numNodes)/(dx*dx)
    lowerDiag = -1.0*np.ones(numCells)/(dx*dx)
    lowerDiag[-1] = 0.0
    upperDiag = -1.0*np.ones(numCells)/(dx*dx)
    upperDiag[0] = 0.0

    rhs = np.array([HeatFlux(pt)/np.exp(logK) for pt in pts])
    rhs[0] = 0.0
    rhs[-1] = 0.0

    stiffMat = sp.diags([lowerDiag, mainDiag, upperDiag], [-1,0,1])
    sol = spla.spsolve(stiffMat, rhs)

    # Interpolate to get the solution at the observation points
    obsSol = np.zeros(obsPts.shape[1])

    for i in range(obsPts.shape[1]):
        minInd = int(np.floor(obsPts[0,i]/dx))
        maxInd = minInd + 1
        minWeight = (obsPts[0,i] - pts[minInd])/(pts[maxInd]-pts[minInd])
        maxWeight = (pts[maxInd] - obsPts[0,i])/(pts[maxInd]-pts[minInd])
        obsSol[i] = minWeight*sol[minInd] + maxWeight*sol[maxInd]

    return obsSol

def SolveGP(obsPts, gp, logK, numSolve):

    solvePts = np.linspace(0,1,numSolve).reshape((1,numSolve))

    # Condition the GP on 0 Dirichlet conditions
    loc = np.array([0.0])
    val = np.array([0.0])
    gp.Condition(loc, val)

    loc = np.array([1.0])
    val = np.array([0.0])
    gp.Condition(loc, val)

    # Condition the GP on second derivative observations
    linOp = mm.IdentityOperator(1)
    obsCov = np.zeros((1,1))
    for i in range(numSolve):
        obsVal = -(1.0/np.exp(logK))*HeatFlux(solvePts[0,i])
        obs = ma.DerivativeObservation(linOp, solvePts[:,i], [obsVal], obsCov, [[0,0]])
        gp.Condition(obs)

    postMean, postVar = gp.Predict(obsPts, ma.GaussianProcess.FullCov)

    return postMean, postVar

spaceDim = 1
numDisc = 100

obsPts = np.array([0.25, 0.75, 0.8]).reshape(1,3)
numObs  = obsPts.shape[1]
obsVar = 5e-4

trueK = 0.6
obsVals = 0.5*(10.0/np.exp(trueK))*obsPts[0,:] - 0.5*(10.0/np.exp(trueK))*obsPts[0,:]*obsPts[0,:]


prior = mm.Gaussian([0.0], [1.0])

numPlot = 500
plotPts = np.linspace(-1,2,numPlot)
gpPostVals = np.zeros(numPlot) # posterior values accounting for discretization error with GP
anPostVals = np.zeros(numPlot) # posterior values using analytic model
fdPostVals = np.zeros(numPlot) # posterior values using finite difference
priorVals = np.zeros(numPlot) # prior values

for i in range(numPlot):
    priorVals[i] = prior.LogDensity([plotPts[i]])

    gpMean, gpCov = SolveGP(obsPts, GetPriorGP(), plotPts[i], numDisc)
    likelihood = mm.Gaussian(gpMean.T, gpCov + obsVar*np.eye(numObs))
    gpPostVals[i] = likelihood.LogDensity(obsVals) + priorVals[i]

    fdMean = SolveFEM(obsPts, plotPts[i], numDisc)
    likelihood = mm.Gaussian(fdMean, obsVar*np.eye(numObs))
    fdPostVals[i] = likelihood.LogDensity(obsVals) + priorVals[i]

    anMean = 0.5*10.0/np.exp(plotPts[i])*obsPts[0,:] - 0.5*10.0/np.exp(plotPts[i])*obsPts[0,:]*obsPts[0,:]
    likelihood = mm.Gaussian(anMean.T, obsVar*np.eye(numObs))
    anPostVals[i] = likelihood.LogDensity(obsVals) + priorVals[i]

plt.figure(figsize=(12,8))
plt.plot(plotPts, np.exp(anPostVals)/np.sum((plotPts[1]-plotPts[0])*np.exp(anPostVals)), linewidth=3, label='Analytic Posterior')
plt.plot(plotPts, np.exp(gpPostVals)/np.sum((plotPts[1]-plotPts[0])*np.exp(gpPostVals)), linewidth=3,label='GP Posterior')
plt.plot(plotPts, np.exp(fdPostVals)/np.sum((plotPts[1]-plotPts[0])*np.exp(fdPostVals)), linewidth=3,label='FD Posterior')
plt.plot(plotPts, np.exp(priorVals), '--k', linewidth=2, label='Prior')
plt.xlabel('$\log(k)$')
plt.ylabel('$\pi(\log(k) | d)$')
plt.title('Posteriors with $N=%d$ nodes in discretization'%numDisc)
plt.legend(loc=9,ncol=4)
plt.show()
