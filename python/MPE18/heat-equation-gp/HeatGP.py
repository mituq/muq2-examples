import pymuqModeling as mm
import pymuqApproximation as ma
import numpy as np

import matplotlib.pyplot as plt

def HeatFlux(x):
    """ Returns the right hand side of the heat equation at a point x. """
    return 10.0


spaceDim = 1

numPlot = 100
plotPts = np.linspace(0,1,numPlot).reshape((1,numPlot))

numSolve = 40
solvePts = np.linspace(0,1,numSolve).reshape((1,numSolve))

var = 4.0
length = 0.3
kern = ma.SquaredExpKernel(spaceDim, var, length)
mu = ma.ZeroMean(spaceDim,1)
gp = ma.GaussianProcess(mu,kern)


priorMean, priorVar = gp.Predict(plotPts, ma.GaussianProcess.DiagonalCov)

# Condition the GP on 0 Dirichlet conditions
loc = np.array([0.0])
val = np.array([0.0])
gp.Condition(loc, val)

loc = np.array([1.0])
val = np.array([0.0])
gp.Condition(loc, val)

logK = 0.6

# Condition the GP on second derivative observations
linOp = mm.IdentityOperator(1)
obsCov = np.zeros((1,1))
for i in range(numSolve):
    obsVal = -1.0*HeatFlux(solvePts[0,i])/np.exp(logK)
    obs = ma.DerivativeObservation(linOp, solvePts[:,i], [obsVal], obsCov, [[0,0]])
    gp.Condition(obs)

postMean, postVar = gp.Predict(plotPts, ma.GaussianProcess.DiagonalCov)

# Plot the prior
plt.fill_between(plotPts[0,:], priorMean[0,:] + np.sqrt(priorVar[0,:]), priorMean[0,:] - np.sqrt(priorVar[0,:]), color='k', alpha=0.3, label='Prior $\mu\pm\sigma$')
plt.plot(plotPts[0,:], priorMean[0,:],'-k',linewidth=3, label='Prior $\mu$')

# Plot the posterior
plt.fill_between(plotPts[0,:], postMean[0,:] + np.sqrt(postVar[0,:]), postMean[0,:] - np.sqrt(postVar[0,:]), color='b', alpha=0.3, label='Posterior $\mu\pm\sigma$')
plt.plot(plotPts[0,:], postMean[0,:],'-b',linewidth=3, label='Posterior $\mu$')

# Plot the analytic solution
plt.plot(plotPts[0,:], 0.5*(HeatFlux(0)/np.exp(logK))*plotPts[0,:] - 0.5*(HeatFlux(0)/np.exp(logK))*plotPts[0,:]*plotPts[0,:], '--g', linewidth=2,label='Analytic')

plt.title('Gaussian Process Solution to $-\partial^2 u / \partial x^2 = 10$')
plt.xlabel('x')
plt.ylabel('u')


plt.ylim([-2.5,2.5])
plt.legend(loc=9,ncol=5)
plt.show()
