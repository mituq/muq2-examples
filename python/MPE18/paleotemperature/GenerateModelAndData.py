import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt
import h5py

Nz = 50#50 # How many nodes to use in space
Nt = 100#100 # How many steps in time

ts = np.linspace(0,1000, Nt) # In years
zs = np.linspace(0,1500, Nz) # In meters

dt = ts[1] - ts[0]
dz = zs[1] - zs[0]

alpha = 100
kair = 1e-1
kgnd = 1e-1

airTemp = -4.5+np.sin(ts/100)
gndTemp = -1.5*np.ones(Nt)

#g = 1.0*kair*airTemp
#f = -1.0*kgnd*gndTemp


rhs = np.zeros(Nz*Nt)
vals = []
rowInds = []
colInds = []

# First, set up the initial conditions as steady state solution
currInd = 0
rhs[0] = -kair * airTemp[0]
rhs[Nz-1] = -kgnd * gndTemp[0]

vals.append(-1.0/dz-kair)
rowInds.append(currInd)
colInds.append(currInd)
vals.append(1.0/dz)
rowInds.append(currInd)
colInds.append(currInd+1)
currInd +=1

for zInd in range(1,Nz-1):
    vals.append(2.0/(dz*dz))
    rowInds.append(currInd)
    colInds.append(currInd)

    vals.append(-1.0/(dz*dz))
    rowInds.append(currInd)
    colInds.append(currInd-1)

    vals.append(-1.0/(dz*dz))
    rowInds.append(currInd)
    colInds.append(currInd+1)
    currInd += 1

vals.append(1.0/dz-kgnd)
rowInds.append(currInd)
colInds.append(currInd)

vals.append(-1.0/dz)
rowInds.append(currInd)
colInds.append(currInd-1)
currInd +=1


# Now set up all other times
for tInd in range(1,Nt):

    # Top
    vals.append(-1.0/dz-kair)
    rowInds.append(currInd)
    colInds.append(currInd)
    vals.append(1.0/dz)
    rowInds.append(currInd)
    colInds.append(currInd+1)

    # this gets added below
    #rhs[currInd] = -kair*airTemp[tInd]

    currInd += 1

    # Middle
    for zInd in range(1,Nz-1):
        rowInds.append(currInd)
        colInds.append(currInd)
        vals.append(1.0/(alpha*dt) + 2.0/(dz*dz))

        rowInds.append(currInd)
        colInds.append(currInd-1)
        vals.append(-1.0/(dz*dz))

        rowInds.append(currInd)
        colInds.append(currInd+1)
        vals.append(-1.0/(dz*dz))

        rowInds.append(currInd)
        colInds.append(currInd-Nz)
        vals.append(-1.0/(alpha*dt))

        currInd += 1

    # Bottom
    vals.append(1.0/dz-kgnd)
    rowInds.append(currInd)
    colInds.append(currInd)

    vals.append(-1.0/dz)
    rowInds.append(currInd)
    colInds.append(currInd-1)
    rhs[currInd] = -kgnd * gndTemp[tInd]

    currInd +=1



A = sp.csr_matrix((vals, (rowInds, colInds)), shape=[Nz*Nt,Nz*Nt])

# Given the large sparse system, extract a small dense matrix mapping f ang g to the final temperature profile
temp = np.zeros((Nz*Nt, Nt))
rowInds = list(range(Nz,Nz*Nt,Nz))
colInds = list(range(1,Nt))
temp[rowInds,colInds] = -1.0*kair

# Solve for the solution from the initial conditions
icSol = spsolve(A,rhs)

# Solve the system the BC impact
modelMat = spsolve(A,temp)
bcSol = modelMat@airTemp

sol = icSol + bcSol

# Extract the matrices for just the last time
modelMatLast = modelMat[(Nt-1)*Nz:Nt*Nz,:]
offsetLast = icSol[(Nt-1)*Nz:Nt*Nz]

ax1 = plt.subplot(211)
ax1.plot(ts,airTemp,linewidth=3)
ax1.set_ylabel('Air Temperature',fontsize=16)
plt.setp(ax1.get_xticklabels(), visible=False)

ax2 = plt.subplot(212, sharex=ax1)
TS, ZZ = np.meshgrid(ts,zs)
plt.contourf(TS,ZZ, np.reshape(sol,(Nt,Nz)).T, 30,cmap='coolwarm')#,vmin=-2.8,vmax=-0.0)
plt.xlabel('Time',fontsize=16)
plt.ylabel('Depth',fontsize=16)
plt.gca().invert_yaxis()
plt.ylim([1000,0])

f = h5py.File('PaleoTempModel.h5','w')
f['/Model/Matrix'] = modelMatLast
f['/Model/Matrix'].attrs['Column Description'] = 'The Nt columns of this matrix represent air temperature at each time.'
f['/Model/Matrix'].attrs['Row Description'] = 'Each row corresoponds to one node in the spatial discretization.  First row is located at ice-air boundary.  Last row is located and ice-gnd boundary.'
f['/Model/Offset'] = offsetLast
f['/Model/InitialCondition'] = rhs[0:Nz]
f['/Model/zs'] = zs
f['/Model/ts'] = ts
f['/Model/AirTemp'] = airTemp
f['/Model/GndTemp'] = gndTemp
f['/Model'].attrs['Nz'] = Nz
f['/Model'].attrs['Nt'] = Nt
f['/Model'].attrs['alpha'] = alpha
f['/Model'].attrs['k_air'] = kair
f['/Model'].attrs['k_gnd'] = kgnd

f['/Data/Vals'] = sol[(Nt-1)*Nz:Nt*Nz]
f['/Data/TrueAirTemp'] = airTemp

f.close()


# plt.figure()
# plt.plot(ts,sol[0::Nz], label='Top')
# plt.plot(ts,sol[int(Nz/2.0)::Nz], label='Middle')
# plt.plot(ts,sol[Nz-1::Nz], label='Bottom')
# plt.xlabel('Time')
# plt.ylabel('Temperature')
# plt.legend()
# #plt.plot(sol[Nz-1::Nz])

# plt.figure()
# plt.plot(sol[0:Nz],zs, label='Time 0')
# plt.plot(sol[9*Nz:10*Nz],zs, label='Time 10')
# plt.plot(sol[19*Nz:20*Nz],zs, label='Time 20')
# plt.plot(sol[29*Nz:30*Nz],zs, label='Time 30')
# plt.plot(sol[39*Nz:40*Nz],zs, label='Time 40')
# plt.plot(sol[49*Nz:50*Nz],zs, label='Time 50')

# plt.xlabel('Temperature')
# plt.ylabel('Position')
# plt.gca().invert_yaxis()

# plt.legend()
# plt.show()
