import numpy as np
import scipy.linalg as la
import scipy.sparse as sp
import scipy.sparse.linalg as spla

import matplotlib.pyplot as plt

class HeatEquation1d:
    """ Defines a discretization of the one dimensional heat equation on [0,1].
    """

    def __init__(self, Nx, dt):
        """ INPUTS:
              Nx: The number of nodes in a finite difference discretization
                  of the heat equation.
              dt: The timestep to take
        """

        # Copy the discretization parameters
        self.Nx = Nx
        self.dx = 1.0/(self.Nx-1)

        self.dt = dt

        # Construct the second order finite difference matrix
        self.A = self._BuildMatrix()

    def NodeLocations(self):
        """ Returns the locations of the finite difference nodes on the interval [0,1] """
        return np.linspace(0,1,self.Nx)

    def Jacobian(self, endTime):
        """ Returns the Jacobian of the solution at time $T$ with respect to the
            initial condition.
        """
        
        Nt = int(np.floor(endTime/self.dt))

        jac = np.eye(self.Nx)

        for i in range(Nt-1):
            rhs = jac + 0.5*self.dt*self.A.dot(jac)
            jac = spla.spsolve(sp.identity(self.Nx) - (0.5*self.dt)*self.A, rhs)
        return jac

    def Solve(self, u0, endTime):
        """ Solves the 1d Heat equation. 
            
            INPUTS:
              u0 : A vector of length Nx containing the initial temperature.
              endTime : A float holding the last time the solution is desired.
            
            RETURNS:
               An (Nx)x(Nt) matrix u containing the solutions at every timestep until endTime.
               u[:,0] contains the initial condition and u[:,-1] contains the solution at endTime.
        """
        Nt = int(np.floor(endTime/self.dt))

        u = np.zeros((self.Nx, Nt))
        u[:,0] = u0

        for i in range(Nt-1):
            rhs = u[:,i] + 0.5*self.dt*self.A.dot(u[:,i])
            u[:,i+1] = spla.spsolve(sp.identity(self.Nx) - (0.5*self.dt)*self.A, rhs)
        return u

    def _BuildMatrix(self):

        rows = []
        cols = []
        vals = []

        dx2 = self.dx * self.dx

        # Set up the interior
        for node in range(1,self.Nx-1):
            rows.append(node)
            cols.append(node-1)
            vals.append(1.0 / dx2)

            rows.append(node)
            cols.append(node)
            vals.append(-2.0 / dx2)

            rows.append(node)
            cols.append(node+1)
            vals.append(1.0 / dx2)

        return sp.csr_matrix((vals, (rows,cols)), shape=(self.Nx,self.Nx))
