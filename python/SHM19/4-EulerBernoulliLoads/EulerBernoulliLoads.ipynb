{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Inferring loads on an Euler Bernoulli beam\n",
    "\n",
    "Here we look at another linear Gaussian inverse problem, but one with a distributed parameter field and PDE forward model.  More specifically, the inverse problem is to infer the forces acting along a cantilevered beam.\n",
    "\n",
    "<div><img src=\"CantileverBeam.png\" width=\"500px\" style=\"padding-bottom:0.5em;\"/><br>Image courtesy of wikimedia.org</div>\n",
    "  \n",
    "\n",
    "\n",
    "### Formulation:\n",
    "\n",
    "Let $u(x)$ denote the vertical deflection of the beam and let $m(x)$ denote the vertial force acting on the beam at point $x$ (positive for upwards, negative for downwards).  We assume that the displacement can be well approximated using Euler-Bernoulli beam theory and thus satisfies the PDE\n",
    "$$\n",
    "\\frac{\\partial^2}{\\partial x^2}\\left[ E(x) \\frac{\\partial^2 u}{\\partial x^2}\\right] = m(x),\n",
    "$$\n",
    "where $E(x)$ is an effective stiffness that depends both on the beam geometry and material properties.  In this lab, we assume $E(x)$ is constant and $E(x)=10^5$.  However, in future labs, $E(x)$ will also be an inference target. \n",
    "\n",
    "For a beam of length $L$, the cantilever boundary conditions take the form\n",
    "$$\n",
    "u(x=0) = 0,\\quad \\left.\\frac{\\partial u}{\\partial x}\\right|_{x=0} = 0\n",
    "$$\n",
    "and\n",
    "$$\n",
    "\\left.\\frac{\\partial^2 u}{\\partial x^2}\\right|_{x=L} = 0, \\quad  \\left.\\frac{\\partial^3 u}{\\partial x^3}\\right|_{x=L} = 0.\n",
    "$$\n",
    "  \n",
    "Discretizing this PDE with finite differences (or finite elements, etc...), we obtain a linear system of the form\n",
    "$$\n",
    "Ku = m,\n",
    "$$\n",
    "where $u\\in\\mathbb{R}^N$ and $m\\in\\mathbb{R}^N$ are vectors containing approximations of $u(x)$ and $m(x)$ at the finite difference nodes.  \n",
    "\n",
    "Only $M$ components of the $N$ dimensional vector $u$ are observed.  To account for this, we introduce a sparse matrix $B$ that extracts $M$ unique components of $u$.  Combining this with an additive Gaussian noise model, we obtain  \n",
    "$$\n",
    "y = BK^{-1} m + \\epsilon,\n",
    "$$\n",
    "where $\\epsilon \\sim N(0,\\Sigma_y)$.\n",
    "\n",
    "## Notes:\n",
    "- With the Gaussian process tools in MUQ, spatial locations are treated as columns of a matrix.  In 1D, this means that that the list of finite difference nodes will be a row vector.  This means that `x.shape` should return $(1,N)$, where $N$ is the number of points in the finite difference discretization.\n",
    "- Reference the Gaussian process and linear regression notebooks for help on MUQ syntax.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.path.insert(0,'/home/fenics/Installations/MUQ_INSTALL/lib')\n",
    "\n",
    "from IPython.display import Image\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import h5py\n",
    "\n",
    "# MUQ Includes\n",
    "import pymuqModeling as mm # Needed for Gaussian distribution\n",
    "import pymuqApproximation as ma # Needed for Gaussian processes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read discretization and observations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = h5py.File('FullProblemDefinition.h5','r')\n",
    "x = np.array( f['/ForwardModel/NodeLocations'] )\n",
    "K = np.array( f['/ForwardModel/SystemMatrix'] )\n",
    "B = np.array( f['/Observations/ObservationMatrix'] )\n",
    "obsData = np.array( f['/Observations/ObservationData'] )\n",
    "\n",
    "trueLoad = np.array( f['/ForwardModel/Loads'])\n",
    "\n",
    "numObs = obsData.shape[0]\n",
    "numPts = x.shape[1]\n",
    "dim = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Construct components of the likelihood"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "obsMat = np.linalg.solve(K,B.T).T # <- BK^{-1}\n",
    "noiseCov = 1e-12 * np.eye(numObs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define a Gaussian Process prior"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "priorVar = 10*10\n",
    "priorLength = 0.5\n",
    "priorNu = 3.0/2.0 # must take the form N+1/2 for zero or odd N (i.e., {0,1,3,5,...})\n",
    "\n",
    "kern1 = ma.MaternKernel(1, 1.0, priorLength, priorNu)\n",
    "kern2 = ma.ConstantKernel(1, priorVar)\n",
    "kern = kern1 + kern2\n",
    "\n",
    "dim =1\n",
    "coDim = 1 # The dimension of the load field at a single point\n",
    "mu = ma.ZeroMean(dim,coDim)\n",
    "\n",
    "priorGP = ma.GaussianProcess(mu,kern)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot prior samples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAA0UAAAF8CAYAAAAAbRD5AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDMuMC4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvOIA7rQAAIABJREFUeJzs3XlwpOlh3/ff0/fduI85sHPs7MweXO4x3F0qEimapoqWaNOVspOoYkmRZW1iJ46tlEqx4iROXHFJdlSVOFbFCsuiKUU0LVlxopjUQUqyzEiidjl7crU7s3MfGNxAA32fT/543r4wwAwwA6AB9PfDeup53qMbD2aG2P7hOV5jrRUAAAAA9CtfrzsAAAAAAL1EKAIAAADQ1whFAAAAAPoaoQgAAABAXyMUAQAAAOhrhCIAAAAAfY1QBAAAAKCvEYoAAAAA9DVCEQAAAIC+RigCAAAA0NcCe/nFjDFflPQ5SfPW2me8c89J+gVJEUk1SX/DWvv6g95rZGTEnjhxYhd7CwAAAOAge+ONNxattaMPum9PQ5GkL0n6eUm/3HHuH0n6H621v2WM+X7v+Hsf9EYnTpzQhQsXdqOPAAAAAA4BY8zNrdy3p9PnrLXflLS8/rSklNdOS7q7l30CAAAA0N/2eqRoI39b0u8YY35OLqR9V4/7AwAAAKCP7IeNFv66pJ+w1h6X9BOSfnGzG40xrxpjLhhjLiwsLOxZBwEAAAAcXvshFP2IpH/ttf+VpJc2u9Fa+wVr7Xlr7fnR0QeulwIAAACAB9oPoeiupE967T8j6XIP+wIAAACgz+z1ltxfkdtZbsQYc0fS35P045L+sTEmIKkk6dW97BMAAACA/ranocha+4ObXHpxL/sBAAAAAE37YfocAAAAAPQMoQgAAABAXyMUAQAAAOhrhCIAAAAAfW1PN1oADhRrpXJWKq64ulaSqsXuulaSqiWpVmzX1ko+v2R8kvF3tH1e299u+wKu+IOSL+jO+YPe+aDkb14PSYGwFIh6dUQKRlztD0nG9PpPCwAA4MAiFOHgaTSkUkYqLEuFRSm/6OrCkpRfksprLrRUiy6kSJKMFxzuU9fLUjHjQlAp49q23rNvc3tMO3iZzvb9zvldwArGpGB0XYl5wat5rbMd2+Qe71wk5c4T1AAAwAFBKMLea9Sl0qoLHpWCF2Dyrq54dbWwLvQseaHHa+9VWAklpMhA+4N+MNpdd47YNM8ZI9mGC2+27r5f29m2XrvmjhtVqV7zjte1GzV3XC9LtUp7dKpWkmpl92fVqEry3nO/hDh/SIqkvT+7dLtEO4/XteMjUuqIGykDAADYQ4QiPJx6rR1sihmptOLVmfvU3v3ltUf/+uGUFBt2H6RjI1572LUjKW/0whvJkORCg71/7Q9J0UH3AT066D6sB0KP3tfdZr3vwTa6i9afW3fcqHnBqtAeWWsG0lppg/PF9rnaBueqBRdyy1kX4vILrmyLkRLjUvqolDoqpY93tI+5OjHmphkCAADsEELRQdYccSksS8VlN+2rsOyta+n8ENzxYVhqr1lZv3bFNqTSmvtQW17z2t5xabU75FSyj9Bx44JLJO1GYprTr0LxjulZMRdM4l7gWR+ADkJY2SumOQVwH+2bUi21Q3NptV2KKx3HHdeKGSk3L+Vm22X6jY3f2xeUBk9II2dcGfbqkSek2NCefpsAAOBwIBT1mrVuylhh0a2H6ZwuVsp4gWRdOGku/i+tSrI96rjpmA41sL06nOI3/Ydd0JtWmBzf3uvqVSk7K63ekdamO+ppae2OqwuL0tJlVy6te310aOOwNHiCaXkAAGBThKKdVi21178UltwITmG5+1zr2FszUys9/NeLpN0Hweig+y15dMh9GDX+DRbW+yRZ98Gza/1K1Z0zxgWWSNrV4aQb0QmnvJGdjnATTkm+fTQygcPBH5QGjruymUpBWr4qLV6Wlq5Iix+228Vl6fZrrnTyBVwwGj0njT0ljT/l6qHTbpQUAAD0NT4NPKo/+afSO1+RCisu6FTz23+PQMRNC2uuiWlOE2uOqnSGk3Cye5E6H+jQb0IxaeIjrnSyVsrOeAHpsqub7cxtF5qWrkgXv9p+jT8kjZz1QtKT0tjTrp06yu55AAD0ET5RP6rcvDTzTvvYF2yvgYkNeWW4XaJD666NuLU0fAADHo0xbve61BHp1Ce7r1WLLhAtXJLm/lSaf9+VzC1p7juudAqnpdGz0ugTbnRp9Jw7Th1jhBQAgEPIWNurNSmP5vz58/bChQu97oa0ctNNgWuGnlCCgAMcFKU1aeGiC0hz77fDUmFp4/uDcReURs56oemcNHZOGjhBWAIAYB8yxrxhrT3/wPsIRQDQwVq3lfjCRTeytHCp3c7Pb/yaUFKaeMZN6Tv6onTsY9LQKX5BAgBAj201FDF9DgA6GeOehZQYk05+ovtaYdlt7NAKTBfdCFNuVrr1LVeaokPSsfPSsZdcffRFtzYQAADsO4QiANiq2JA09YornXLz0ux33PrC6TekO9+WcnPS5a+7Ikkybrrd8Y+5kaRjH3PT8Jh2BwBAzzF9DgB2mrXS6m3p9uvSnQsuJM2847a/7xROtafbHfuYG1HiAbQAAOwYps8BQK8YIw1MufKRv+TOVUvS7LsuIN35tnT72+6BtNf+rStNw49LR89LR56TJj/q1imFk735PgAA6BOEIgDYC8GIdPwlV5rW7nojSd6I0t232s9Tevdftu8bOu0C0uSzXlD6qHuuGQAA2BFMnwOA/aJedWuTpt9w0+1m33UbOayfdie5ZyZNftTtejfiPU9p+HEXvgAAgCSmzwHAweMPSkdfcKWpVpEWPpBm3nVBaeYdae49N/Vu7Y506Wvte41PGnjMPUOpGZSabXa+AwBgU4wUAcBB06i7KXYz77iHzS5424SvXJdsY+PXJCelkTNuNGnotKuHH5cGH3NhDACAQ4iRIgA4rHx+NwI0erb7fK0sLV2VFi+5oNSqP5SyM65c/2b3a4xfGjzRDknDp93I0pHn2OABANA3CEUAcFgEwtL4U650atSlzE1p6Vp7I4elKy5Ard6Wlq+6cvl32q8xPmn8GfdMphPfLZ36FFPwAACHFtPnAKCfVYvS8vXuoDT/p24Nk6237/MFpRP/nvTEZ6VzP+C2GwcAYJ/b6vQ5QhEA4F6VvDT9pnTrT6Qrv+u2De9cr3T0Rempz0tP/gVp6GTv+gkAwH0QigAAO6ew7MLRxa9Kl78hVQvta5MfdQHpqb/o1iQBALBPEIoAALujUnAB6f3fkD78bamSa18bf8aFo6c+L40+0bs+AgAgQhEAYC9US9LV33cB6dJvSuW19rXRJ70RpM9LY09KxvSunwCAvkQoAgDsrVpZuvbvpPf/HzfNrrTavjbyhHT2z0lnvk86/jLPRgIA7AlCEQCgd2oV6cY33QjSB1+Visvta+GUdPpTLiA9/mel5ETv+gkAONQIRQCA/aFek27+kXT5626ThsVL3dcnnnUB6cz3ScfOu4fTAgCwA/ZlKDLGfFHS5yTNW2uf6Tj/NyX9F5Jqkr5mrf2pB70XoQgADqiVGy4cXf6GdP2bUq3YvhYdlE5/2htF+rQUH+lZNwEAB99+DUWfkJST9MvNUGSM+ZSkvyvpB6y1ZWPMmLV2/kHvRSgCgEOgWpRu/JF05RvSh78jrVzvuGjc85CaD4xlswYAwDbty1AkScaYE5K+2hGKfk3SF6y1v7ud9yEUAcAhtHTVm2b3denGH0r1Svva4Enpyc9J5z4nHfsY0+wAAA90kELR25J+Q9JnJZUk/aS19tsPeh9CEQAccpW8283u0m9Kl35LKiy2r8VHpbPf7wLSqU9KgXDv+gkA2Le2GooCe9GZBwhIGpT0iqSPSfo1Y8wpu0FaM8a8KulVSZqamtrTTgIA9lgoLp37flcaden2a9LFr0kf/Bspc1N685dcCSWkM59xAenMZ6RIutc9BwAcMPthpOi3Jf2stfYPvOOrkl6x1i7c730YKQKAPmWtNPen7llIF78qzX6nfc0XlE5+wq1BOvcDbPcNAH3uIE2f+88kHbHW/vfGmCck/Z6kqY1GijoRigAAkqSVm24E6eLXpFt/LNlG+9qxj3kB6c9LI4/3ro8AgJ7Yl6HIGPMVSd8raUTSnKS/J+n/lPRFSc9JqsitKfr9B70XoQgAcI/8ovThb7uAdPX3pVqpfW3krAtIT35OOvICO9kBQB/Yl6FoJxGKAAD3VclLV37PBaQPf0sqrbavJY+0p9id+G7JH+xdPwEAu4ZQBABAU70q3fyj9jS7ten2tUi6/Syk05+Wwone9RMAsKMIRQAAbMRa6e5b3kYNX5MWLravBSLSyU+60aOpj0uTH5UCod71FQDwSAhFAABsxeKVdkC683r3tUBUOnbeBaSpV6TjL0nhZG/6CQDYNkIRAADblZ2Vrv5b6da3XFn8sPu68bkNG448Lx15ztXjz0ihWG/6CwC4L0IRAACPKr8o3fqTdkiaeUdq1LrvMX5p7Elp8jkvKL0gjT8tBSO96TMAoIVQBADATquW3INj774p3X3brU1auCjZevd9voA09lR7NOnI89LY06xPAoA9RigCAGAvVArS3HsuIDXLwiVJ6/776g95Qen5jqD0JNuBA8AuIhQBANAr5Zw0+512SJp5W1q8rHuDUliaeKYdkiafk0bPSf5AT7oNAIfNVkMRP3UBANhp4YT02MddaSqtSbPvdowovS0tX5Wm33ClKRCVJj7S3sxh7Elp6LQUSe399wEAfYJQBADAXoik3POPTnx3+1wx4zZvaI4m3X1LWrnhtgZfvz14fNSFo+HT0tApV4+ek4bPMLIEAI+In6IAAPRKdEA69UlXmgrLXkDyQtLiZWnlupRfcOX2n3S/RyDi1ipNPitNPOseODv2FNuEA02NulSvSvWKq21DramsrWUknVNbjWTMBrU6jn1SMMYvJA4R/iYBANhPYkPS6T/jSlOjIWXvSktX3ZS7pavS8jW3wUPmlrcb3pvt+43PjSC1gtKzbqtwpuBhP6iV3bq78ppUybl2xTuuFl2pldxujzWvNM+1zhfd+7TOlzuCT8Vtnd9s28bufS+BqHugczjh6lBy3XFCCqfWHSfb7UhKig1Lweju9RFbwkYLAAAcZMWM29Rh9l1p5l1XL1y6d5tw43Pbgk+9Ih37mNvgYeQJdr9Dt3rVPZ8rP+/qUsathyuvSZW8Fz4qrq6VpXp5Xbuy8blaqf269f8294I/7P6t+wKuSO3RH3WMAlkryd6nllc33J/H+s1THlYwJsVG3C9F4iMuKHUdj7gptKlJKTEuBcI783X7ALvPAQDQr6pFaf6DdlCaedvVjWr3ff6QNHpWGn/GlYlnXHCKj3R8YMShUS26kcXMLbd2LXNTWr0j5ebb0zOLK7vfD1/g3lGV5ghKKO6mhAYi7gHIgagLAMHoA86H3b9nf7C79gUln393/j1bK1ULUjm7buTLO65kO65l26NhrZGxrFRalQpLLjBuR2xYSk5KyQmv7minJqX0lAtU/P+YUAQAADpUi9L0m25N0t23pNn33FqljUQH3SjSyBlp8ISUPi6ljkrpY67mIbT7U60ird72gs9NaeVmu525JeXmHvwextcelYgPu38L4ZQUSbvgEgi5EOIPeUEk7OpmKNnKOV+AD+udrHUhKb/o1hQWFl1Qyi92tJfc6N3ajPt73MpoWzglDT7mAlJ8WIoOuaAUjHnBsTNEhtz6qA3PB124XP/36fPt/p/NDiAUAQCA+ytnpbn33dqkufdcUJr/wP2Ge1NGSoy5gJQ+1h2YmiU+yofe3VCrSGvTXvDZIPxk795//Ywv6P5+Bh+TBqakAa9OjLu/0/ioC0E+/959T9i+Rt0FpuxMR5l19Zp3vHLzAf8/3gG+gBd2Q27UL+5N+YuPSI99l/TCD+/u198inlMEAADuL5yUpl52pcla95voxQ9dydx2U6yaJev9pjo31/18pU7+sJQ+6oWl464dH23/pjo23C7skufUa25qVX7B+zP3prllbrdDUHZG913DYnzuz7sZdgYf624nJwk8h4HPLyXHXdFzG99jrRthWrnh/n9bXHajUMUVN2rc3ImvuRlFveqm164/12zXKh1tb61Yo+ZKNe/ed/VW99ffJ6FoqwhFAACgzRhvbcKEdPIT916v16TcbHdQapY1ry6uuN3xlq89+OsFIl5AGvJCU0dgagUor25O5Qon99eH+0bDBZrSarsurboNCprtsrdZQWmTulp48NcxPil5xAs+G4Sf9DE2zoBjjBuxiY9Ixx44SLJ91nqhydtIo7zWPeUvfXznv+YuIxQBAICt8wfa0+Q2U85507yaYWnafWAqLK+rl9yuZGvTrmxHMO62M25tbxx3oaGlY/qeMevWSYTb7eYal9bC/EDHOgq/VCncP+iUVt00xEfdhcz43PcRG26HnvSUV3vHqaOEHuwPxnjry0JSWG7q3NDJXvfqkRCKAADAzgon3K52o2fvf19z965mQCosdwem4nL3teKKt5tX1k3Zqea9KWX7QHMzgmYdadbNc6mOOu0CUOe5UIJ1WEAPEYoAAEBvGONGeEJxNw1sqxqNjq2Ps+3tjpujNes3kbLWrZfoesCn166VvbUUNa+udLSrrm/hjoATWR9+0vtvOh+AbSMUAQCAg8Xn88JJqtc9AXBIHIwNxgEAAABglxCKAAAAAPQ1QhEAAACAvkYoAgAAANDXCEUAAAAA+hqhCAAAAEBfIxQBAAAA6GuEIgAAAAB9jVAEAAAAoK8RigAAAAD0NUIRAAAAgL5GKAIAAADQ1/Y0FBljvmiMmTfGvLfBtZ80xlhjzMhe9gkAAABAf9vrkaIvSfrs+pPGmOOSPiPp1h73BwAAAECf29NQZK39pqTlDS79L5J+SpLdy/4AAAAAQM/XFBlj/oKkaWvtO73uCwAAAID+E+jlFzfGxCT9XUnft8X7X5X0qiRNTU3tYs8AAAAA9ItejxSdlnRS0jvGmBuSjkl60xgzsdHN1tovWGvPW2vPj46O7mE3AQAAABxWPR0pstZ+R9JY89gLRuettYs96xQAAACAvrLXW3J/RdK3JJ01xtwxxvzYXn59AAAAAFhvT0eKrLU/+IDrJ/aoKwAAAAAgqfdrigAAAACgpwhFAAAAAPoaoQgAAABAXyMUAQAAAOhrhCIAAAAAfY1QBAAAAKCvEYoAAAAA9DVCEQAAAIC+RigCAAAA0NcIRQAAAAD6GqEIAAAAQF8jFAEAAADoa4QiAAAAAH2NUAQAAACgrxGKAAAAAPQ1QhEAAACAvkYoAgAAANDXCEUAAAAA+hqhCAAAAEBfIxQBAAAA6GuEIgAAAAB9jVAEAAAAoK8RigAAAAD0NUIRAAAAgL5GKAIAAADQ1whFAAAAAPoaoQgAAABAXyMUAQAAAOhrhCIAAAAAfY1QBAAAAKCvEYoAAAAA9DVCEQAAAIC+RigCAAAA0NcIRQAAAAD62p6GImPMF40x88aY9zrO/c/GmIvGmHeNMf+3MWZgL/sEAAAAoL/t9UjRlyR9dt25b0h6xlr7rKQPJf30HvcJAAAAQB/b01Bkrf2mpOV1575ura15h38i6dhe9gkAAABAf9tva4r+qqTf6nUnAAAAAPSPfROKjDF/V1JN0pfvc8+rxpgLxpgLCwsLe9c5AAAAAIfWvghFxpgfkfQ5Sf+xtdZudp+19gvW2vPW2vOjo6N710EAAAAAh1ag1x0wxnxW0n8t6ZPW2kKv+wMAAACgv+z1ltxfkfQtSWeNMXeMMT8m6eclJSV9wxjztjHmF/ayTwAAAAD6256OFFlrf3CD07+4l30AAAAAgE77Yk0RAAAAAPQKoQgAAABAXyMUAQAAAOhrhCIAAAAAfY1QBAAAAKCvEYoAAAAA9DVCEQAAAIC+RigCAAAA0NcIRQAAAAD6GqEIAAAAQF8LPOgGY8wntvOG1tpvPnx3AAAAAGBvPTAUSfoDSdZrm472ZvyP0iEAAAAA2EtbCUWf6mgPSPonkt6T9C8lzUkal/SDkp6W9J/vdAcBAAAAYDc9MBRZa/9ds22M+ZKkr1tr/9q6237ZGPOLkv59Sf9mR3sIAAAAALtouxstfF7Sr25y7Ve96wAAAABwYGw3FPkkPb7JtTNiPREAAACAA2a7oehrkn7GGPOXjTF+STLG+I0x/4Gk/0nSV3e6gwAAAACwm7ay0UKn/1LScbmpcjVjzIqkQe99/tC7DgAAAAAHxrZCkbV2UdL3GGM+I+kVSZOSZiR9y1r7u7vQPwAAAADYVdsdKZIkWWu/IekbO9wXAAAAANhz211TBAAAAACHyrZDkTHmVWPMW8aYgjGmvr7sRicBAAAAYLdsKxQZY35Y0j+R9G1JEUn/XNKvSFqTdFXS39/pDgIAAADAbtruSNHflvQzkv66d/y/W2t/RNIpSUVJSzvYNwAAAADYddsNRWckfVNSwyshSbLWrkj6B5L+1o72DgAAAAB22XZDUVGSz1prJc3KjRA15SQd2amOAQAAAMBe2O6W3N+R9Lik35X0/0n6b4wx1yXVJP0Pki7uaO8AAAAAYJdtNxR9Qe3Rof9OLhz9oXeclfQXd6hfAAAAALAnthWKrLW/2tG+Yox5WtJ3SYpK+mNr7eIO9w8AAAAAdtV2R4q6WGvzkr6xQ30BAAAAgD237VBkjIlJ+quSPilpSG4b7j+Q9CVrbWFHewcAAAAAu2y7D2+dkPSmpP9N0nlJMUkfk/Tzkt4wxozveA8BAAAAYBdtd0vufyRpUNL3WGtPWms/bq09Kem7JQ1I+oc73UEAAAAA2E3bDUV/TtJPW2v/qPOktfaPJf23kn7gfi82xnzRGDNvjHmv49yQMeYbxpjLXj24zT4BAAAAwEPbbihKSLq7ybU73vX7+ZKkz64793ck/Z619oyk3/OOAQAAAGBPbDcUXZL0Q5tc+yt6wMNbrbXflLS87vTnJf2S1/4l8awjAAAAAHtou7vP/ZykX/Y2VPgXkmYkTUj6jyT9WW0emO5n3Fo7I0nW2hljzNhDvAcAAAAAPJTtPrz1V7wtuf++pH/WcWlO0n9qrf0XO9m59Ywxr0p6VZKmpqZ280sBAAAA6BPbnT4na+0XJB2R9LSk7/Hqo5JuGGPefYg+zBljJiXJq+fv97WtteettedHR0cf4ksBAAAAQLdthyJJstY2rLUfWGv/yKsbktJyAWm7/l9JP+K1f0TSbzxMnwAAAADgYTxUKHpYxpivSPqWpLPGmDvGmB+T9LOSPmOMuSzpM94xAAAAAOyJ7W608EistT+4yaVP72U/AAAAAKBpT0eKAAAAAGC/eeBIkTHm1Bbfa+IR+wIAAAAAe24r0+euSLJbuM9s8T4AAAAA2De2Eop+dNd7AQAAAAA98sBQZK39pb3oCAAAAAD0AhstAAAAAOhrhCIAAAAAfY1QBAAAAKCvEYoAAAAA9DVCEQAAAIC+RigCAAAA0NcIRQAAAAD6GqEIAAAAQF8jFAEAAADoa4QiAAAAAH2NUAQAAACgrxGKAAAAAPQ1QhEAAACAvkYoAgAAANDXCEUAAAAA+hqhCAAAAEBfIxQBAAAA6GuEIgAAAAB9jVAEAAAAoK8RigAAAAD0NUIRAAAAgL4W6HUHAAAAsDOstcqVaypW6irXGqrWG6rUG6rWrCr1hiq1hqysAj6f/D7J7/Mp4DPyGaOA36t9Rn6vBHxGPq82xshaq4ZVV20lNTqPrRQJ+pWOBhUK8Pt3HAyEIgAAgH2s0bBazJU1t1bWYr6spVxFS7mylvIVLea843xZy7mKFvMVVWqNXne5JRL0aTAW0ngqoiMDEU2mo5pMe/VAREcHohpLhmWM6XVX0ecIRQAAAD1UqtY1s1rS3UxR0ytFTWdcuevVM5mSKvWtB51o0K94OKBwwKeg3ygU8Cno97VqIzeyU2tYNRqurncWa1Wrt9vN842Glc9nZIzkM0Y+I0mu9pn2eUkqVutaLVZVqjY0s1rSzGpJb9/evL8nR+I6ORrXaa8+NZLQydG4UpHgI//5AltBKAIAANhlpWpdN5byurGY17VFV19fzOvGUkEL2fIDXz8UD2kiFdFIMqzheMiVRFjDiY52PKThREix0P74eGetVaFS13K+otk1F/pmVkuaXW23pzNFLecren9mTe/PrN3zHiOJsE6NxHVqNK6TI3GdGk3o5EhcU0MxpuZhR+2P/9cAAAAcAuVaXVfn87o0t6aLM1ldnM3qynxO05nipq/x+4wmUhEdHYzq6IArRwaireMjA5F9E3S2wxijeDigeDig40OxTe9bLVR1bTGnawsuKHa2F3NlLebKev3Gctdr/D6jE8MxnZtI6YnxpM5OJHR2IqWpoZj8PqbiYfuMtbbXfXgo58+ftxcuXOh1NwAAQB9qNKymM0VdnM3q0uyaV2d1bTGveuPez1YBn9HUUEwnRtyIx4mRuE4Ox3ViJKaJVEQBP6Me6zUaVjNrJV1baIekqws5XV/MazpT1EYfYcMBn86MJ3R2PKWzEwk9MZ7UuYmUxlOsW+pXxpg3rLXnH3Tfwfu1AwAAwB5ayVda4efSnBv9+XA2q3ylfs+9PiOdGonr7ERSZyeSOjeR1BPjSU0NxQg+2+TzmdbI2fecGe26VqrWdWU+p0uzWX04l9WlORdKZ1ZLem96Te9Nd0/FG0mE9ZGjKX3k2IA+cjStZ4+lNZ6K7OW3g32OkSIAAADPaqGqt+9k9ObNFb19O6OLs2uaW9t4zc9IIqxzHeHn3ERKZ8YTigT9e9xrNK0Wq7rshaQPZ12A/WBmTWul2j33jibD+sjRdLsQlA6lrY4U7ZtQZIz5CUl/TZKV9B1JP2qtLW12P6EIAAA8inrD6vJ8Vm/dciHordsZXZnP3XNfNOjXE+MJnZtItQLQ2YmkhhPhHvQa22Wt1e3lot6dzug706v6zp1VfWd6VdkNgtJYMygdc0HpzFhSRwejrFM6wA5UKDLGHJX0h5KestYWjTG/Juk3rbVf2uw1hCIAALBV1ro1QB/MZPXunYzevLWid26vKlfu/mAc8vv0zNGUnp8a1PNTA3rmSFpTQzH5+FB8qFhrdXOp4EJghDedAAAgAElEQVSSF5Tem15VtnxvUAoFfDoxHNOpkYROjbod8E6NxnV6JKF0jC3D97uDuKYoIClqjKlKikm62+P+AACAA8Raq+V8RbdXirq1XNDt5YJuLRV0Yym/6RSqowNRPT81oBe8EPTUkZTCAaa/HXbGGJ3wNrz48x89Islt7HBzuaB372T03vSq3pte07XFnObWyvpwLqcP5+4dRRyOh1xQ8gLTaS8wHR+KKcgasgNlX4Qia+20MebnJN2SVJT0dWvt13vcLQAAsM8UKjXdXi7q9nJBt1cKrr3iAtDt5cKGmx80DcVDenIyqWeOpPX81KBemBrQGGtI4PH5jHuI7Ehcn3/uaOt8rlzT9QW3VfjVhXzXbnhL+YqW8hV9+8ZK13sFfEZT3ujS6dHmc5ZcYBqOh9gJbx/aL9PnBiX9X5L+Q0kZSf9K0q9ba39l3X2vSnpVkqampl68efPmXncVAADsoEqtoWypqly5pmypply5ppxXZ0tV3V0teQGoqDvLBS3lK/d9v6T3TJypoZimhmOt9rmJpMaSbMuMndNoWM2ulXStGZjmc7q2mNe1hfx9n0uVjATcFDwvgD02HNNjw3E9NhTTQCzIv9EddtDWFP1lSZ+11v6Yd/zDkl6x1v6NzV7DmiIAAPanar2hhWxZc2slzWfLrqyVNL9W1ny2pMVcRSuFilbylfuO7Gwk5Pfp2GBUx4ZiOj4Y1fGhmI4PxnR8KKqpoZjSUT5UoveKlfo9D6J1gSm34QYPTfGQXxPpiCbTUU2mI5pMRzThtSe8Y/6Nb89BW1N0S9IrxpiY3PS5T0si8QAAsE8VKjXdWSnqxmJet5bdup2bSwXdXCpoOlPc8AGmGwn4jFLRoBLhgOLhgJLhgBKRgBJePZ6M6PhQO/yMJcNseoB9Lxry66kjKT11JNV13lqrpXzFC0ouMN1cKujmckG3lvLKV+q6upDX1YX85u8d9LdC0kRncEq54yMDUQ0y4rRt+yIUWWtfM8b8uqQ3JdUkvSXpC73tFQAA/aVUrWs5X9FSrqKlfFlLuYoWcuXWCM9CtqwFb+Rn/a5tnYxxWxuPpyIaS4Y1lgprLBlp1SOJkIbjYQ3GXRjiwxv6hTFGI4mwRhJhvXRyqOuatVZrxZpm1oqaWS1pJlPS7Kprz66VvHNF5St1N+q0uHlwSkYCOjOW0ONeOTOW1ONjCR0diPJLhU3si+lzD4PpcwAAbE25Vtf8Wrn14ar5QWturaTZVTedbTlfuW/QWS/k9+nIQESPDcd1YjimKa9+bDiuY4NRHmAK7JJsqarZ1ZLurnaEptVSq76bKW64tbgkRYI+nR1PulGsSTeSdW4ipXh4X4yT7IqDNn0OAAA8gly5pg/nsvpwNquLs1ndXi54Aaj0wM0JmoJ+o6G4G8UZToQ0FA9pNOFGekaT3mhP0rVZ1wD0RjISVDIS1Jnx5IbXrbVayJV1ZT53T5nPlvXOnVW9c2e1db8x0onhuJ6aTOnMeEInR+I6MexKPz2HiVAEAMABUqk1dG0xp0uzWV2azerDOReC7qxsvtuV32c0ngxrvLn+IOUWbo+nI5pIRTSaDGsoHlIqwlQ24KAzxni/wIjou06PdF1bLVR1cXZN78+s6U/vrun9u2u6PJ/V9UW3GYS+0/1eg7GgTozEdXI43nquk2vHlIwcrsBEKAIAYB9qNKymM0Vd7Ag+l2bXdG0hr9oGmxgE/UanRxM6N5HUExNJnRqJazId1UQ6opFEWH7WEQB9Lx0L6uVTw3r51HDrXKXW0JX5nN6fWdPVhZxueAHp5lJBK4WqVm5l9NatzD3vNZIIuRGlkbimhmJd6wePDEQ1FA/t5bf2yAhFAAD02GKu3Br5uTSb1aW5rC7PZTfcrtoY6bHhmM6OJ3V2wivjSZ0YiSvo9/Wg9wAOslDAt+lOefPZsq4v5l1QWnL1jUW32+RirqLFXEUXbq7c856ffXpCv/BDL+7Vt7AjCEUAAOyBSq2hW8sFXVvIuWeWLLSfYbLZmp/RZNiN/Iy3w8+Z8YRiIf7zDWB3GWM0nopoPBXRKx0jS1L7wbXNsHQ3U9Tcmns22UK2rFOj8R71+uHxUxUA+kyjYZUt11Su1VWuNlSu1VWqNlSuNVSu1lWuNVSq1lXyzhcr7Xa5WnfXqg3vnNeu1lWpNxT0+xQN+hUN+hUL+zUQDWkwFtRA3KujIQ3EghqMhzQQDSoW8h/INSz1hlWhUlOxUle+Ule+XFOuXPO2sy5rIefqpVxFi7myFnJl3VnZ/Nk9yXBAT3jhpzMEHbTpJwD6g89ndGQgqiMDUX3X4yMPfsEBQCgCgEPCWqvlfKW9Peu6rZcXs+7ZMyuF6pYfrLnbQn6fBmJBr3QEp3hQg95x2gtWg/GQUpGgAn6jgM/I5zPyGyO/zyvG3Pf5G9ZalaoNFSo1FSou0BUqrqyVqlotVrVacHWmWNFqsaZMoaJ82d2fr9RUKLu6VG1s+3s1Rjo2GNWp0YROjcR1ejTu2qNxTaQiBzIcAsBhQSgCgH2uXKtrdrWk6ZWi7q6WtJgrt0ch8u0RiaV8WdX61sJOPORXNORXOOBXOOhTOOBXJOhTOOBTJOhXxDuOBP2KBN097lzn+fa5sPfaSs2qVK2rWK0rV6opU6xopVBVplBRplDVilc32+VaQ/Pew0B3gjFqhSOfkYxc0LCyDxVk7ice8isWDrg6FFA87HfbWSfCGomHNJIMt7a2HkmEdGwwxrN7AGCfIhQBQI/lyzXdWSlqOlPQ9EpRdzJFTa8UNe3VC7mytvqc7XQ06LZc9rZeHk+169Gke4r6YCykUGB/LMgvVuouOOW94FTsDE73Bqq1Uk31hlWt3lDDumls9YZV3braWqlmrbTJSFg44FMs5Kb3Rb0wEw35lYoElI6GlI66UatmnYoGlQwHWqGnWUcCfp4KDwCHCKEIAPZIvlzTlfmcPpzL6vJ8TpfnsvpwLqfpzObPl5Ekn5Em0xEdHXTzt8eSYQ0nwhqOhzScaD9oczgeVjR0sEYioiG/oqGoJtPRHXm/RsOq1rBqWFc6hQN+tqUGAGyIUAQAO6wz/DTr+4WfkN+nY0NRHR2I6thgVEfSUR0ddMdHB6OaSEUUYKvlLfH5jEIEHwDANhGKAOAh1BtW0ytF3VjK68ZSvvU08MsPCD+nRuN6fCyhJ8aTemI8ocfHkjoxHCP0AADQQ4QiANhEo2F1d7WoG4uFjofWuWcy3F4ubLqpQdBvdGokoTPjLvycGUvozDjhBwCA/YpQBKDvWGu1VnQ7ozUX8C/mKppbc1tX382UdGMpr1vLBVVqm+9YNp4K68RwXCdH4joxEteJYTcKRPgBAOBgIRQBOHTKtbruZtwW1ndWCrrj1Xe95/XMrpZUvk/Y6TSaDOvkcFwnRmI6MRL32nE9NhxTLMSPUAAADgP+iw7gwClV67qbcVtW3+kKPq49n33wFtbxkF+D8ZAGYyENxIIajoc0no5oPOm2s54aciEoEebHJAAAhx3/tQewr1TrDS3myppbK2t+raS5bFkz68LPgx706TPSEW8nt2ODMR0bbO/mNp5yoYewAwAAmvhUAGDH1RtWuVJNa6Wq1kpVZUs1rRVdnS1VtebVWe+ebKmmpVxF89mSlvKVB47y+H1Gk+lIV+hphZ+BqCbSEQVZ0wMAALaIUARgW/Llmqa9qWt3M0VNr7j6bqak6UxRmUJF+Ur9od/fGLeOZzwV1lgyovFUWOOpiI53jPjw3B4AALCTCEUAWsq1uhaybura3UyxVaa9wHM3U9Rqsbql90qGA0pGAkpFg0pGAkpGgkp59UbnB2MhTaQjGo6HCDwAAGBPEYqAQ6hWbyhTrGq1WFWuVFOuXFPWq3OlqvKVulaLVS1ky5rPljS/VtZ8trylwBMK+HR0wE1TOzIQ0ZGBqFu/MxDV5EBUQ/GQEuGA/D6zB98pAADAoyMUAfuAtVa1hlW13lC1ZlWpN1zbK6VqQ4VKXYVKTflyvf18nXxFywXXXs5XlClUtJyvaK1Ue6h+BHxGI4mwxlJhHUlHvcATcSFo0B0Px0MyhsADAAAOD0IRsAXlmhtZWS240ZdMsy5WtVqotNvFqkrVump1F3JqjUa7XW94dftatdZQte5C0E4yRkpHgxqIBpWMBJUIB5SIBJQMBxT32olwQKPJsMaSbu3OWCqsoVhIPkZ4AABAnyEUoa+VqvV7Ngy4kylqJlPSSjPsFKoqVh9+44Ct8vuMgn6joN+nkN+noN+nYKB9nAgHFAsHFAv6NRALaiAW0mAs2HrWzlDcnRuKhZSKBpm+BgAAsEWEIhx6lVpDt5bzurqQ1/XFvK4t5HRtIa8bS3kt5ipbeo+g3ygdDbbKQCykgWhQqWhQA7HmOVdHgn4F/T4Xcny+VthxtTsO+I0CPl8rBDXPAwAAYO8Rih7Rt28s673pVQ0nwhqJhzSUCGk4HtZgLMgOWnusVK3rynxOl+ez+nAup8tzWV2Zz+n2SlH1xsYPvgn4jCaba2YGYjo6EOlYOxNWOuamoMVCftbRAAAAHFKEokf0u+/P6f/45rV7zhsjb0pTSKnW9sPNLYgDSnnbEA/EQhr2wtRQ3E19Ikx1q9UbWilUtZQvazlX0VLebSbg6rJmV8u6PJ/VreXChg/9NEY6PhTVqZGETo7EdXo0rlOjrj2eijBCAwAA0OcIRY/oxccG9cMff0xLuYoWc+XWB/YVbxew5fzWpmd1SkeDLiB5ZbiznQhpKO4WxA/EgkrHgkqGAwdqFKNSa2ilUNFSruKCTt61O4NOu+12VtuKgM/oxGhcT4wn9PhY0qsTOjEcVyTo3+XvCgAAAAcVoegRfd/TE/q+pyfuOd85upEt1ZQtVbVW9OqSe2bMWsltqdz88N8MU6veLmbXF/Nb6oPfZ1o7jTWnew3EQq31L6GAW6gf8BsF/D6FvPUsAb/xzrt2c/2LK5Lf55PfmA3PVep1FSp15ct1Fas1t1102dsyulJXsdK8Xrsn6GS3uV1056jbUDykkeaoWjys4XhIo8lwK/yEAoyyAQAAYHsIRbsk4PdpNBnWaDK8rdfVG1aZQqU1krK8LjQ1A0amUPVKRflK/aFHpXrB7zMajHWMgCXa7eFEuN326oFYiCluAAAA2DWEon3G7zMuGCTCenxsa6+p1Bre6FKlHZaKLjCtFauq1N1DQWv1hqoNq2qt0X5QaN09R6fqPUen3rCuWKtGwz1PZ6NzoYBPsZBfsVDAqzvb3eeG4kE3quOFn1QkyLNwAAAAsG8Qig6BUODhRqUAAAAASCzAAAAAANDXCEUAAAAA+tq+CUXGmAFjzK8bYy4aYz4wxny8130CAAAAcPjtpzVF/1jSb1tr/5IxJiQp1usOAQAAADj89kUoMsakJH1C0n8iSdbaiqSDsb80AAAAgANtv0yfOyVpQdI/N8a8ZYz5Z8aYeK87BQAAAODw2y+hKCDpBUn/1Fr7vKS8pL+z/iZjzKvGmAvGmAsLCwt73UcAAAAAh9B+CUV3JN2x1r7mHf+6XEjqYq39grX2vLX2/Ojo6J52EAAAAMDhtC9CkbV2VtJtY8xZ79SnJb3fwy4BAAAA6BP7YqMFz9+U9GVv57lrkn60x/0BAAAA0Af2TSiy1r4t6Xyv+wEAAACgv+yL6XMAAAAA0CuEIgAAAAB9jVAEAAAAoK8RigAAAAD0NUIRAAAAgL5GKAIAAADQ1whFAAAAAPoaoQgAAABAXyMUAQAAAOhrhCIAAAAAfY1QBAAAAKCvEYoAAAAA9DVCEQAAAIC+Fuh1B3CwlOtlZStZrVXWlK1kla1klavmVK1XVWvUVG1UJUkBX0B+41fAF1A8GFc8GFcimFAimFA85Nohf6jH3w0AAABAKOpL1lrlq3ktlZa0VFxq1SvlFeUqOeWquXbgWXdcaVR2rB9BX9CFJC80xYNxJUKJVoBKhpKuBJNKR9KaiE1oIj6hkeiIAj7+6QIAAGBn8MnyEKg36spWslqtrCpTzmi13F0vFbvDz1JpSeV6+aG+VsAXUCqUapVkKKlYMKaQP6SACSjgC8gYo3qj3ho5KtQKylVyylfzylW9upJTtVHVSnlFK+WVbfXBb/wajY22QtJkYlKn0qdaJRFKPNT3BgAAgP5EKNonrLUq1ortAFHLa7W82irNkLM+8KxWVrVWXpOV3dbXiwaiGooMaTg6rOHIsIajwxoMDyoZSioRSrRGaBKhRKtOhVIK+8MyxuzI91tpVJSr5FSoFpSr5tqBqZq7Z4RqubSsufycZvIzWiotaTY/q9n8rLRw73uPx8Z1euC0TqVP6fTAaZ0bOqfHBx5XJBB55H4DAADg8CEUPSJrrUr1knKVXGutTdf0M+8Dfr6a7y61vPKV7rphGw/dj2QoqYHwgNKhtNKRdKs9EB7oCj7NOhaM7eCfwvYZYxT2hxWOhjUcHd7Wayv1iuYKc61gNJ2b1rXVa7qWuabrq9c1V5jTXGFOf3z3j1uv8Ru/TqZP6tzQua6SDqd3+lsDAADAAWOs3d4Iw35x/vx5e+HChV53Qz/7+s/qyx98eUfeK+KPKBaMtdbXpENppcIpDYQHXMgJp5UOp+85ToVSrLHx1Bt1TeemdTVzVVdXr+pK5oouLV/StdVrG4bOxwce18uTL+uliZd0fuK8UqFUD3oNAACA3WCMecNae/5B9/FJ+hHFg3GFfKH2pgBe6dwooLlxQDwYVywYu2dzgeb5oC/Y62/nwPP7/JpKTWkqNaVP6VOt86VaSZdXLuuD5Q90afmSLi5f1KWVS7qSuaIrmSv68gdfls/49OTQk3p58mW9PPGynht7rucjagAAANh9jBQ9ooZtyGd43NNBVKlX9O7Cu3p99nW9NvOa3l18V7VGrXU94Avo2ZFn9crkK3pp8iU9O/Ksgn6CKwAAwEGx1ZEiQhHgKVQLemv+Lb02+5pen3ld7y+937WBRTQQ1fNjz+uliZf0yuQrOjd0Tn6fv4c9BgAAwP0QioBHtFpe1YW5C3p95nW9Pvu6rmSudF1PBpM6P3G+tSbp8YHHd2RnPgAAAOwMQhGwwxaLi/r27Lf12sxrem3mNd3J3em6PhQZ0ssTL+ulyZf08sTLOpY8RkgCAADoIUIRsMumc9OtUaTXZl7TQrH7oUmT8Um9NPGSXhx/Uc+PPa/HUo8RkgAAAPYQoQjYQ9ZaXV+73gpJr8++rtXyatc9Q5EhPT/2fKs8OfQkGzcAAADsIkIR0EMN29CHKx/q9ZnX9fbC23pz7k0tlZa67on4I3pm5Bk9P/a8Xhh/QR8d/aiSoWSPegwAAHD4EIqAfcRaq9vZ23pr/i29Nf+W3px/U9dXr3fd4zM+PTH4hF4Ye0EvjL+gF8Ze0GhstEc9BgAAOPgIRcA+t1Ja0dvzb+ut+bf0xvwben/p/a7nJEnS8eRxvTD2gl4cf1EvjL+gqeQU65IAAAC2iFAEHDDFWlHvLb6nN+be0Jtzb+qdhXdUqBW67hmJjuj5seddSBp7QU8MPsGzkgAAADZBKAIOuFqjpkvLl1xImn9Tb82/peXSctc9iWBCHx37qF4ccyNJz4w8o7A/3KMeAwAA7C+EIuCQae5w9+bcm67Mv6np3HTXPUFfUOeGzuns0Fk9OfSkzg2d05nBM4oGoj3qNQAAQO8QioA+MJufbQWkN+be0JXMlXvu8RmfTqRO6OzQWZ1On9aRxBEdTRzVkcQRjUZHmX4HAMAGGrahaqOqar2qSqOiar2qaqPdbtiGGrahuq13ta21CvlDigaiigQiXXXQx6M49tqBDEXGGL+kC5KmrbWfu9+9hCLgXqvlVV1avqSLyxddWbmoa5lrqtv6hvcHfAFNxifbQSl+hNAEADiwrLXKVrPKlDJaLi0rU85orbKmUq2kUq2kcr2sUr2kcs2r62VXaq5uvnalvKJsJbvj/QuYgGLBmJKhpJKhpBLBhBKhhJLBpBKhhBLBhDvfcS4ZSnZdjwaibLq0DVsNRYG96Mw2/C1JH0hK9bojwEGUDqf10uRLemnypda5cr2sK5krurh0Ubeyt3Q3d1d3c3c1nZvWUmlJt7O3dTt7e8P3IzQBwOFWbVRVqVdUqpVUqVfUUEMhX0ghf0hBX1BBf1ABE9iTD+EN21CpVlKxVlSpXlKxWmy3a0UVqgXlqjnlq3llK1llyhmtlFaUKbcDUKaUUc3WHvzFtijkCynoD7ra+/MI+oIK+ALyG798xudqn08++eQzrlQalfb34tXFWlE1W9NaZU1rlbWH7pPf+LsDVEewagaqiD+ikN/9PYb9YQV9QYX94da5kC/UfbzBfT7j27E/x4Ng34QiY8wxST8g6R9I+q963B3g0Aj7w3p6+Gk9Pfz0PdeKtaJm8jNdQWm7oWksOqZUOKVUyCud7VBKyVCydS4aiLZ+GAf9wa4fwv32wxcAHlW1XnXBoLzSGt3IlDIuHHScX6ustUZHWiMj9bIq9cqmMwk6GRkFfcHWh+eAL9AVnDoDVDM8NO8N+oLyG38r2DRLZ1DoDEI7IR6MayA8oKHIkAbCA0qH04oEIor4Iwr7wwoHwq12JOCd80oilNBAeECD4UElQ8kd/8VftV5VrppTrpJTtprtqnPVnLKVrLKVbKvdeb75ulK9pNXyqlbLqzvat/Waf89hf7gVBtcHwYAJtI+9QOj3+fXxyY/rx5/98V3t307bN6FI0v8q6ackJXvdEaBfRANRnUqf0qn0qQ2vbyU03c3f1d383UfuS/MHasDX/gHrN375fd755g9en7/r3qAvqFggpljQlXgg7tobnIsH44oFYooGo133MdoFYD8qVAutn7d3cndaP4Onc9Oazk3vyPQuv/G3Q0EgLCPTGj1qrqep2ZoqjYoqjYpU3YFv7D4i/khr/c36NTmxQKw1KpIIJpQOp1vBZzAyqMHwoAYiA/t6F9agP6hB/6AGI4MP/R6dwWqtuuaC07qQValXWsG3Und/d/ecq1dUbpRVrVfvubdcL6vWqKnWqN3zeJCtGI+NP/T31yv7IhQZYz4nad5a+4Yx5nvvc9+rkl6VpKmpqT3qHdC/thKaFguLWquuaa281poSsFm7OT2juUi1+UO62qiqbuuq27qqjV3+L+4GIv5IV5BqhqfmuYHwgMbj4xqPjWsiPqHx2LhGoiOEKQCPpFqvaiY/0wo809nprhC0/jEM6/mNX+lwuhUGumovKKTDaTdS0jEyEvKHWtOrAr4HfxSsN9zP5vVhqfNnePNne61Ra324bm5KUG/Uu4JONBBVxO8dB9vtSCDCrIEt2Ilg9SDW2tbfazMw1Rq11qYSnXW9Ub/n/FBkaNf6tlv2xUYLxpifkfRDkmqSInJriv61tfavbPYaNloADg9rrWq25n6gej9c1/+Q7TzXqm1dlXpFhVpBxWpRhVpB+Wq+XVcLKtQKrXr9uea9D8Nv/BqNjWo81h2WJhOTmohNaDIxqaHIEP+BB/pcrVHTdG5a11evt8qt7C1N56Y1X5hXwzY2fW3AF9CRuFvHeTR5VEcTR3Uscay1rnMwMsjPGOABDtRGC9ban5b005LkjRT95P0CEYDDxRijoPG2Kd3jwZfmwt71QalQLShfcyFqubSsufyc5gpzms3Paq4wp+XSsmbzs5rNz2763kFfUBPxCU3GJzURn9BwZFipcKo1x30gPKBUKKV0OK14MK6IP6KAb+8WNHcGz5qtqdFoB9DOtQfr1yJ0HtdtXT7jk5FxtTH3HnuLjzuvrT8fDURbC4abuzLFAjF2WMKBka/mdWP1hq6tXusKQDezN1VrbLzw32d8XZvZHEsca4Wfo4mjGouNEXqAPbIvQhEA9IrP+Fprj7SNZ9xW6hXNFea6wtJsflazhVnN5ec0k59Rppy570YVGzEyCvvDrTDQDBDN/8m01181g0dnqdt6V7jpfH5GrVFrjbwdBD7jUzwYVzrkrRuIDHQtnh6MDN5znAqlCFLYNdZazRXmukLP9TVXzxfmN33dZHxSJ9MnXUmd1FRq6v9v7+5jLKvPAo5/n3nbndlll+7L8NItu5jSxgpGGqKtosXUGMofkGrbgBKLEvuHVtPaoNbSSoU2UmOMNVSptVJLi638IZS0KVFbKVoIG2mxQIkUAQnQnZ3ZXXZm7t29M/P4xzn37p1hZ+fuMHNf9n4/yck5597fzn1m8uydee75nefHrs27OHPTmQwPum6N1A26Yvrcajh9TlK3m63N8uLsi42Caao6xUtHXuLgkYMcOnKosX7GwSMHqcxVODJ3ZE1bya7keM0r6s0tmrsxLe3O1Hw+GIMkyUIuNPYLuUBmLvv4AuXz5fFCLjA7N1vcIHz02I3ClbnKSX9PIwMj7BzbyY7RHYyPjS/abx3Z2rgKtWl4U6MTYn2RRYup/lZbqDFZmWTf7D4mZifYVyn3s/uYqBT756efX3bK7cjACLu37ubcLeceK4C2nsueLXuKD10kdURPTZ+TpFPR2PDYCRtVHE/9JmWAJBvFRf0YOFZgkMwvzC8qOhotUcviZmmx09w6tdvVFmrMHJ3h0NFDHKgeeNl6JAeqBxa1Ip6qTjFTm2l05joZwwPDbNu4jW0bt7F9dDvbN25v7Bv3jm06g/HRcT/Z7yH1ltWNFtVlDk1WJtlX2XesAJrdx1R1imTlD4pfteFVi4qe+nb2prNtviL1MIsiSeoiQwNDLXWD6gfDA8PFlLmNp7N7y+6W/s1sbZb9lf1MVCaYmJ1YtK+v/3G4dpiZozONzllH549Sna8W0yFnf7jia2zbuK3RYKO+/sno0GjjKlq9s1a9s1d9ocfGFsW+vihmYyHIsoitLwo5ODDIUBx7fDAGT5mrWfML88zMzTBzdIaZ2syx47kZpo9ONxqjTNemG/f6Tdemqc5VF11hbL4COZ/zi9oO19d3aVUQjSuL46Pj7Bzbyc6xnYyPjhePjY1zxtgZnL7x9HX8yUjqFH/zSrP2vxkAAAoESURBVJJOGWPDY5wzfA7nbDm5ZRsqcxWmqlNMViaZrEwWx9XieKIy0bh3bH9lP1PVKaaqUzw+9fg6fRfLa16/ayiG2DC04eWr2tePm9ZzqU8b3DyymS3DWxgdHi2uKlI0umheeDHJxvokzVstj61ZU3+sMldpdHWcrk0XBU4L21ot0tnKz6u5ZfXpG47dl1YvdMbHxtk5upPto9v9QELqY/7vlyT1vdGh0UbHrxOZX5hnsjrZKJIOHz1Mdb5Kda7c5o/tmxc/bBQWC7VFx/Xzeqv5uZxb1Hp+0XnTRtnF+XDtMPsr+9vwE1pbQRTrgQ0Xi3G+7HhojM0jxXFjG9rE6NAoAwPHirlFjUYYYGRwhA2DGxgZHGkUhL0wVVRS51kUSZLUosGBwcbVhQu4oK2vXZ8iVi+Yags1qnNVpmtlg4pyhfv6vr66ffPz9SmElblK4+s19uU9asCi6X716X3N0wDrxxsHNy4uXJbZNg9vXlT0uEinpG5jUSRJUg+IiOJ+JIYa63lt3bCVMzijs4FJ0inAj2kkSZIk9TWLIkmSJEl9zaJIkiRJUl+zKJIkSZLU1yyKJEmSJPU1iyJJkiRJfc2iSJIkSVJfsyiSJEmS1NcsiiRJkiT1NYsiSZIkSX3NokiSJElSX7MokiRJktTXLIokSZIk9bXIzE7HsCoRMQE80+k4SjuA/Z0OQj3HvNFqmTtaDfNGq2HeaDW6KW92Z+bOlQb1bFHUTSJib2Ze1Ok41FvMG62WuaPVMG+0GuaNVqMX88bpc5IkSZL6mkWRJEmSpL5mUbQ2Pt3pANSTzButlrmj1TBvtBrmjVaj5/LGe4okSZIk9TWvFEmSJEnqaxZFJyEiLo2IJyLiyYj4w+M8vyEivlQ+/2BE7Gl/lOo2LeTN70XEYxHxSET8a0Ts7kSc6i4r5U3TuHdEREZET3X50fpoJW8i4l3le86jEfHFdseo7tPC76lzIuIbEfFw+bvqsk7Eqe4SEZ+NiH0R8b1lno+I+GSZV49ExBvbHePJsChqUUQMArcAbwPeAFwVEW9YMuxa4EBmvhb4C+Dm9kapbtNi3jwMXJSZPw7cCXyivVGq27SYN0TEacDvAg+2N0J1o1byJiLOAz4I/Exm/hjwvrYHqq7S4vvN9cCXM/NC4ErgU+2NUl3qNuDSEzz/NuC8cnsP8NdtiGnVLIpa95PAk5n5VGYeBf4RuGLJmCuAz5XHdwJvjYhoY4zqPivmTWZ+IzNny9MHgF1tjlHdp5X3G4AbKYroajuDU9dqJW9+E7glMw8AZOa+Nseo7tNK3iSwpTzeCjzfxvjUpTLzPmDqBEOuAP4hCw8Ap0fEWe2J7uRZFLXu1cD/NZ0/Vz523DGZOQccAra3JTp1q1byptm1wNfWNSL1ghXzJiIuBF6Tmfe0MzB1tVbeb14HvC4i/iMiHoiIE33Kq/7QSt7cAFwdEc8BXwV+pz2hqced7N9AHTXU6QB6yPGu+Cxt3dfKGPWXlnMiIq4GLgLesq4RqRecMG8iYoBiiu417QpIPaGV95shiqksl1Bclf5WRJyfmQfXOTZ1r1by5irgtsz884h4M/D5Mm8W1j889bCe+rvYK0Wtew54TdP5Ll5++bgxJiKGKC4xn+iyok59reQNEfELwIeAyzPzSJtiU/daKW9OA84HvhkRTwNvAu622ULfa/X31F2ZWcvM/wWeoCiS1L9ayZtrgS8DZOa3gY3AjrZEp17W0t9A3cKiqHUPAedFxLkRMUJxo+HdS8bcDby7PH4H8G/pQlD9bsW8KadB3UpREDm/X7BC3mTmoczckZl7MnMPxb1ol2fm3s6Eqy7Ryu+pfwZ+HiAidlBMp3uqrVGq27SSN88CbwWIiB+lKIom2hqletHdwK+VXejeBBzKzBc6HdRynD7Xosyci4j3Al8HBoHPZuajEfEnwN7MvBv4O4pLyk9SXCG6snMRqxu0mDd/BmwG/qnsy/FsZl7esaDVcS3mjbRIi3nzdeAXI+IxYB64LjMnOxe1Oq3FvPkA8LcR8X6K6U/X+KGvIuIOiqm4O8r7zf4YGAbIzL+huP/sMuBJYBb49c5E2powpyVJkiT1M6fPSZIkSeprFkWSJEmS+ppFkSRJkqS+ZlEkSZIkqa9ZFEmSJEnqaxZFkqQ1FxHXREQ2bYcj4rsR8d5yceu1fK2nI+K2pvNLIuKGiBhYMm5PGcs1a/n6kqTe5zpFkqT19E6KVc23lMd/BYwDH1nD13g78FLT+SUU62XcBCw0Pf4C8GbgB2v42pKkU4BFkSRpPX0nM58sj++NiNcC72MNi6LMfLjFcUeAB9bqdSVJpw6nz0mS2ukh4LSIGI+I4Yi4qZz+drTc3xQRw/XBETEUETdGxA8iohoR+yPi/oi4uGlMY/pcRNxAcZUIoFafvlc+d9zpcxFxdTm1r/71Px8RZy0Z83RE3B4RV0bE4xExExF7m+OQJPUurxRJktrpXGAemAY+B7wL+DhwP8XUtuuBHwF+pRz/B8D7gQ8B36GYhncRsG2Zr/8ZYBdwLXBx+VrLioj3ALcCXwI+CJxdxvNTEfHGzJxuGv6zwOuBDwNV4EbgnojYk5kHW/v2JUndyKJIkrSeBsvGCqdRFEC/BHyFovC5CvhoZt5Qjr03IuaBGyPiTzPzEYpC6d7M/Mumr/mV5V4sM5+LiOfK0wczc265sRExSFHYfDMzr2x6/PvAt4DfAD7Z9E+2AD+RmQfKcS9SXPm6DPjiiX8MkqRu5vQ5SdJ6+j5QA6aATwFfoCg2fq58/vYl4+vnbyn3DwGXRcTHIuLiiBhZw9heT9H04QvND2bm/cAzTTHUfbteEJX+u9yfs4YxSZI6wCtFkqT19HaK7nOHgWcyswoQEfXpby8sGf9iua8//3GKqWpXA38ETEfEncB1mbn/Fca2XAz1OJZO0ZtqPsnMIxEBsPEVxiFJ6jCvFEmS1tP3MnNvZj5RL4hK9QLjzCXj6+eTAJlZy8ybM/MC4CyK+4t+GbhlDWJbLob6Y5Nr8BqSpB5gUSRJ6oR/L/dXLnn8V8v9fUv/QWa+mJmfAf4FOP8EX/tIuR9dIYYngB8ujSEifhrY3RSjJOkU5/Q5SVLbZeajEXEHcEPZiOE/KZoqfBi4o2yyQETcBXwX+C/gAHAhcClFx7jlPFbuPxARXwPmM3PvcWKYj4iPALdGxO0U9zO9GvgY8D/A37/y71SS1AssiiRJnfJu4CmKxgvXA88DNwMfbRpzH/BO4LeBMeBZ4BMUhcty7qFo6vBbFIvERrm9TGZ+OiJmgeuAuyhahX8V+P0l7bglSaewyMxOxyBJkiRJHeM9RZIkSZL6mkWRJEmSpL5mUSRJkiSpr1kUSZIkSeprFkWSJEmS+ppFkSRJkqS+ZlEkSZIkqa9ZFEmSJEnqaxZFkiRJkvra/wPZ1Df34decRgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 1008x432 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.figure(figsize=(14,6))\n",
    "\n",
    "numSamps = 3\n",
    "for i in range(numSamps):\n",
    "    samp = priorGP.Sample(x)\n",
    "    plt.plot(x[0,:], samp[0,:], linewidth=2)\n",
    "    \n",
    "plt.xlabel('Position',fontsize=16)\n",
    "plt.ylabel('Load',fontsize=16)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute posterior\n",
    "> **Question 1:** Use the Gaussian class in MUQ to compute the posterior mean and covariance.  \n",
    "\n",
    "*Hint: Reference the previous LinearGaussian example for syntax.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot posterior\n",
    "> **Question 2:** Plot the posterior distribution ($\\mu(x)\\pm\\sigma(x)$) over the inferred loads.\n",
    "\n",
    "*Hint: Try adapting the `fill_between` code that we used in the earlier LinearGaussian example.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
