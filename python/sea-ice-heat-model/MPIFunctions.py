# python linear algebra package
import numpy as np

# ParCer includes
import pyParCer as pc

def BroadcastESS(comm, root, posteriorSamps):
    # collect the ess from each processor
    ess = np.array([0.0]*3)
    if root==comm.GetRank():
        ess = posteriorSamps.ESS()
    comm.Bcast(ess, root)

    return ess

def BroadcastRunningCovariance(comm, root, posteriorSamps):
    # collect the running covariance from each processor
    runCov = [np.array([[0.0]*3]*3)]*posteriorSamps.size()
    if root==comm.GetRank():
        runCov = posteriorSamps.RunningCovariance()
    for j in range(len(runCov)):
        dum = runCov[j]
        comm.Bcast(dum, root)
        runCov[j] = np.copy(dum)

    return runCov

def BroadcastCumulativeRefinements(comm, root, posteriorSamps):
    # collect the running covariance from each processor
    cumRefine = np.array([0.0]*posteriorSamps.size())
    if root==comm.GetRank():
        cumRefine = cumRefine + posteriorSamps.GetMeta('cumulative kappa refinement') [0]
        cumRefine = cumRefine + posteriorSamps.GetMeta('cumulative beta refinement') [0]
        cumRefine = cumRefine + posteriorSamps.GetMeta('cumulative gamma refinement') [0]
    comm.Bcast(cumRefine, root)

    return cumRefine
