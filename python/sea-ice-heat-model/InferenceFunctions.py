# python linear algebra package
import numpy as np

# import hdf5 to write to file
import h5py

# MUQ Includes
import pymuqModeling as mm
import pymuqOptimization as mo # optimization

# import the forward model
from ForwardModel import BuoyModel

def Prior():
    # the hyperparameters
    priorMean = np.log([0.9, 4.5, 2.75])
    priorVar = [0.5, 0.5, 0.75]

    # create the prior
    return mm.Gaussian(priorMean, priorVar)

def Likelihood():
    # the file where the data are stored
    file = h5py.File('../generate-data/data.h5', 'r')

    # get domain information the sensor locations
    D = file['/'].attrs['domain length'] # domain length
    zobs = file['/sensor locations'].value # observation locations
    n = len(zobs) # number of observations

    data = file['/noisy data'].value
    noiseVar = file['/noisy data'].attrs['noise variance']

    # create the forward model
    model = BuoyModel(zobs, D)

    # create the likelihood
    likelihood = mm.Gaussian(data, noiseVar*np.eye(n))

    return model, likelihood

def Graph(prior, model, likelihood):
    # create the WorkGraph
    graph = mm.WorkGraph()

    # add the nodes
    graph.AddNode(mm.IdentityOperator(3), 'parameters')
    graph.AddNode(prior.AsDensity(), 'prior')
    graph.AddNode(model, 'forward model')
    graph.AddNode(likelihood.AsDensity(), 'likelihood')
    graph.AddNode(mm.DensityProduct(2), 'posterior')

    # add the edges
    graph.AddEdge('parameters', 0, 'prior', 0)
    graph.AddEdge('parameters', 0, 'forward model', 0)
    graph.AddEdge('forward model', 0, 'likelihood', 0)
    graph.AddEdge('prior', 0, 'posterior', 0)
    graph.AddEdge('likelihood', 0, 'posterior', 1)

    return graph

def ComputeMAP(posterior, guess):
    # cost function for optimization
    cost = mo.ModPieceCostFunction(posterior)

    optimizationOptions = dict()
    optimizationOptions['Ftol.AbsoluteTolerance'] = 1.0e-8
    optimizationOptions['Ftol.RelativeTolerance'] = 1.0e-8
    optimizationOptions['Xtol.AbsoluteTolerance'] = 1.0e-8
    optimizationOptions['Xtol.RelativeTolerance'] = 1.0e-8
    optimizationOptions['Minimize'] = False
    optimizationOptions['Algorithm'] = 'BOBYQA'

    opt = mo.Optimization(cost, optimizationOptions)
    return opt.Solve([guess])

def MCMCOptions(Nsamps):
    proposalOptions = dict()
    proposalOptions['Method'] = 'AMProposal'
    proposalOptions['ProposalVariance'] = 0.01
    proposalOptions['AdaptSteps'] = 100
    proposalOptions['AdaptStart'] = 100
    proposalOptions['AdaptScale'] = 1.0

    kernelOptions = dict()
    kernelOptions['Method'] = 'MHKernel'
    kernelOptions['Proposal'] = 'ProposalBlock'
    kernelOptions['ProposalBlock'] = proposalOptions

    options = dict()
    options['NumSamples'] = Nsamps
    options['ThinIncrement'] = 1
    options['BurnIn'] = 0
    options['KernelList'] = 'Kernel'
    options['PrintLevel'] = 3
    options['Kernel'] = kernelOptions

    return options

def UpdateExpectation(currentEstimate, newSample, Ncurrent):
    return (float(Ncurrent)*currentEstimate+newSample)/float(Ncurrent+1)
