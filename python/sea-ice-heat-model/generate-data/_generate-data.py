import sys
sys.path.append('..')

# python linear algebra package
import numpy as np

# import hdf5 to write to file
import h5py

# import matplotlib so that we can make figures
import matplotlib.pyplot as plt

# import the forward model
from ForwardModel import BuoyModel

# MUQ Includes
import pymuqModeling as mm # modeling probability distributions

n = 25 # number of observations
D = 8.0 # length of the domain
zobs = np.linspace(0.0, D, num=n) # observation locations

# true parameter values
ki_true = 1.0 # thermal conductivity of sea ice
kw_true = 5.0 # thermal conductivity of the ocean
h_true = 3.0 # true ice thickness
theta = np.log([ki_true, kw_true, h_true])

sigy = 0.1 # variance of the noise

# create the forward model
model = BuoyModel(zobs, D)

# evaluate the forward model
soln = model.Evaluate([theta]) [0]

# add the noise to the true solution
noise = mm.Gaussian([0.0]*n, sigy*np.eye(n))
data = soln+noise.Sample()

# write to file
file = h5py.File('data.h5', 'w')
file['/'].attrs['domain length'] = D
file['/sensor locations'] = zobs
file['/true solution'] = soln
file['/true solution'].attrs['true thermal conductivity of sea ice'] = ki_true
file['/true solution'].attrs['true thermal conductivity of the ocean'] = kw_true
file['/true solution'].attrs['true ice thickness'] = h_true
file['/noisy data'] = data
file['/noisy data'].attrs['noise variance'] = sigy

# plot the solution and data
fig = plt.figure()
ax = plt.gca()
ax.plot(soln, zobs, color='#bd0026', linewidth=2) # true forward model solution
ax.plot(data, zobs, 'o', markersize=3, color='#bd0026') # data
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.set_ylabel('$z$ position (meters)', fontsize=16)
ax.set_xlabel('Temperature $T$ (Kelvin)', fontsize=16)
ax.set_ylim([zobs[0], zobs[-1]])
plt.savefig('fig_data.png', format='png', bbox_inches='tight')
