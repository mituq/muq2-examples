import sys
sys.path.insert(0, '..')

# to manipulate files
import os

# python linear algebra package
import numpy as np

# import hdf5 to write to file
import h5py

# import matplotlib so that we can make figures
import matplotlib.pyplot as plt

# import functions to help with inference
from InferenceFunctions import *

# import functions to help with mpi
from MPIFunctions import *

# MUQ Includes
import pymuqModeling as mm # modeling probability distributions
import pymuqSamplingAlgorithms as ms # characterizing probability distributions

# ParCer includes
import pyParCer as pc

# create a global communicator
commGlobal = pc.Communicator()

# create the prior
prior = Prior()

# create the forward model and likelihood
model, likelihood = Likelihood()

# create the WorkGraph
graph = Graph(prior, model, likelihood)

# visualize the graph
if commGlobal.GetRank()==0:
    graph.Visualize('PosteriorGraph.png')

# create the posterior mod piece
posterior = graph.CreateModPiece("posterior")

# compute the map
map = ComputeMAP(posterior, prior.GetMean())

def RunMCMC(posterior, map, Nsamps, save, commGlobal):
    # define the sampling problem
    problem = ms.SamplingProblem(posterior)

    # create MCMC object
    mcmc = ms.SingleChainMCMC(MCMCOptions(Nsamps), problem)

    # run MCMC
    posteriorSamps = mcmc.Run(map[0])

    # if we want to save the data
    if save:
        dataset = '/example chain' # data set name
        # save one process' chain
        if commGlobal.GetRank()==0:
            posteriorSamps.WriteToFile('results.h5', dataset)

    runCov = list()
    ess = list()
    for i in range(commGlobal.GetSize()):
        # collect the ess from each processor
        ess = ess + [BroadcastESS(commGlobal, i, posteriorSamps)]

        # collect the running covariance from each processor
        runCov = runCov+[BroadcastRunningCovariance(commGlobal, i, posteriorSamps)]

    # return the ess and the running covariance
    return ess, runCov

Ntrials = 5 # number of trials to run
Nsamps = 100000 # number of samples per chain

# each trial will return the running covariance and the ess
runningCov = [None]*Ntrials
ess = [None]*Ntrials

# initialize the "true" covariance
cov = np.array([[0.0]*3]*3)

for i in range(Ntrials):
    # run mcmc
    ess[i], runningCov[i] = RunMCMC(posterior, map, Nsamps, i==0, commGlobal)

    # update the truth
    for j in range(commGlobal.GetSize()):
        cov = UpdateExpectation(cov, runningCov[i][j][-1], commGlobal.GetSize()*i+j)

# compute the expected error
expectedError = np.array([0.0]*len(runningCov[0][0]))
for i in range(Ntrials): # loop through the number of tirals
    for j in range(commGlobal.GetSize()): # loop through the number of chains per trial
        for t in range(len(runningCov[i][j])): # update the expected error at each step
            expectedError[t] = UpdateExpectation(expectedError[t], np.linalg.norm(runningCov[i][j][t]-cov), commGlobal.GetSize()*i+j)

# write to file (only using the first processor)
if commGlobal.GetRank()==0:
    file = h5py.File('results.h5', 'a')

    file['/true covariance'] = cov
    file['/'].attrs['number of trials'] = Ntrials
    file['/'].attrs['number of processors'] = commGlobal.GetSize()

    for t in range(Ntrials): # loop through the number of tirals
        for i in range(commGlobal.GetSize()): # loop through the number of chains/trial
            dataset = '/trial '+str(t)+'/rank '+str(i)

            # write the effect sample size for each trial
            if dataset+'/effective sample size' in file:
                del file[dataset+'/effective sample size']
            file[dataset+'/effective sample size'] = ess[t][i]

    # write the expected error for all trials
    if '/expected error' in file:
        del file['/expected error']
    file['/expected error'] = expectedError
