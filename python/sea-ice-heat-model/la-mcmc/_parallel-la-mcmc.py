import sys
sys.path.insert(0, '..')

# to manipulate files
import os

# python linear algebra package
import numpy as np

# import hdf5 to write to file
import h5py

# import matplotlib so that we can make figures
import matplotlib.pyplot as plt

# import functions to help with inference
from InferenceFunctions import *

# import functions to help with mpi
from MPIFunctions import *

# MUQ Includes
import pymuqModeling as mm # modeling probability distributions
import pymuqSamplingAlgorithms as ms # characterizing probability distributions

# ParCer includes
import pyParCer as pc

# create a global communicator
commGlobal = pc.Communicator()

# number of chains sharing model evaluations
Nchains = 5
assert(commGlobal.GetSize()%Nchains==0)

# number of groups per trial
Ngroups = int(commGlobal.GetSize()/Nchains)

# create the communicator between chains that share evaluations
group = 0
while commGlobal.GetRank()>=(group+1)*Nchains:
    group = group+1
chain = commGlobal.GetRank()%Nchains
commChain = pc.Communicator(group, chain) # (color, key)

# create the prior
prior = Prior()

# create the forward model and likelihood
model, likelihood = Likelihood()

# create the WorkGraph
graph = Graph(prior, model, likelihood)

# visualize the graph
if commGlobal.GetRank()==0:
    graph.Visualize('PosteriorGraph.png')

# create the posterior mod piece
posterior = graph.CreateModPiece("posterior")

# compute the map
map = ComputeMAP(posterior, prior.GetMean())

def RunMCMC(posterior, map, Nsamps, save, Ngroups, Nchains, commChain, commGlobal):
    regressionOptions = dict()
    regressionOptions['NumNeighbors'] = 25
    regressionOptions['Order'] = 3
    regressionOptions['MaxPoisednessRadius'] = 1.0

    problemOptions = dict()
    problemOptions['RegressionOptions'] = 'Regression'
    problemOptions['Regression'] = regressionOptions

    problemOptions['BetaScale'] = 0.0
    problemOptions['BetaExponent'] = 1.0

    problemOptions['FirstLevelLength'] = 1.0

    problemOptions['GammaScale'] = 0.0075
    problemOptions['GammaExponent'] = 1.0
    problemOptions['MaximumGammaRefine'] = 1

    problemOptions['TargetMax'] = map[1]

    problemOptions['EtaScale'] = 0.0
    problemOptions['EtaExponent'] = 1.0

    problemOptions['LambdaScale'] = 25.0

    # define the sampling problem
    problem = ms.ExpensiveSamplingProblem(posterior, problemOptions, commChain)

    # create MCMC object
    mcmc = ms.SingleChainMCMC(MCMCOptions(Nsamps), problem)

    # run MCMC
    posteriorSamps = mcmc.Run(map[0])

    # if we want to save the data
    if save:
        # save one group's chain
        if group==0:
            for i in range(Nchains):
                commChain.Barrier() # only one can access file at a time
                if chain==i:
                    dataset = '/example group/chain '+str(i) # data set name
                    posteriorSamps.WriteToFile('results-parallel.h5', dataset)

    runCov = list()
    ess = list()
    cumRefine = list()
    for i in range(commGlobal.GetSize()):
        # collect the ess from each processor
        ess = ess + [BroadcastESS(commGlobal, i, posteriorSamps)]

        # collect the running covariance from each processor
        runCov = runCov+[BroadcastRunningCovariance(commGlobal, i, posteriorSamps)]

        # collect the total number of refinements
        cumRefine = cumRefine+[BroadcastCumulativeRefinements(commGlobal, i, posteriorSamps)]

    return ess, runCov, cumRefine

# get the true covariance
file = h5py.File('../am-mcmc/results.h5', 'r')
cov = file['/true covariance'].value

Ntrials = 5 # number of trials to run
Nsamps = 100000 # number of samples per chain

# each trial will return the running covariance and the ess and the cumulative refinements
runningCov = [None]*Ntrials
ess = [None]*Ntrials
cumRefine = [None]*Ntrials

for i in range(Ntrials):
    # run mcmc
    ess[i], runningCov[i], cumRefine[i] = RunMCMC(posterior, map, Nsamps, i==0, Ngroups, Nchains, commChain, commGlobal)

expectedError = np.array([0.0]*len(runningCov[0][0]))
expectedRefinements = np.array([0.0]*len(cumRefine[0][0]))
for i in range(Ntrials): # loop through the number of tirals
    for j in range(commGlobal.GetSize()): # loop through the number of chains per trial
        expectedRefinements = UpdateExpectation(expectedRefinements, cumRefine[i][j], commGlobal.GetSize()*i+j)
        for t in range(len(runningCov[i][j])): # update the expected error at each step
            expectedError[t] = UpdateExpectation(expectedError[t], np.linalg.norm(runningCov[i][j][t]-cov), commGlobal.GetSize()*i+j)

# write to file (only using the first processor)
if commGlobal.GetRank()==0:
    file = h5py.File('results-parallel.h5', 'a')

    file['/'].attrs['number of chains per group'] = Nchains
    file['/'].attrs['number of trials'] = Ntrials
    file['/'].attrs['number of processors'] = commGlobal.GetSize()

    for t in range(Ntrials): # loop through the number of tirals
        for i in range(commGlobal.GetSize()): # loop through the number of chains/trial
            dataset = '/trial '+str(t)+'/rank '+str(i)

            # create the dataset
            if not dataset in file:
                file.create_group(dataset)

            # compute the group for rank i
            group = 0
            while i>=(group+1)*Nchains:
                group = group+1

            # write the group and chain for rank i
            file[dataset].attrs['group'] = group
            file[dataset].attrs['chain'] = i%Nchains

            # write the effect sample size for each trial
            if dataset+'/effective sample size' in file:
                del file[dataset+'/effective sample size']
            file[dataset+'/effective sample size'] = ess[t][i]

            # write the number of cumulative refinements
            file[dataset].attrs['cumulative refinements'] = cumRefine[t][i][-1]

    # write the expected error for all trials
    if '/expected error' in file:
        del file['/expected error']
    file['/expected error'] = expectedError

    # write the expected number of refinements for all trials
    if '/expected refinements' in file:
        del file['/expected refinements']
    file['/expected refinements'] = expectedRefinements
