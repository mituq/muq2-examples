# to manipulate figure folders
import os, shutil

# python linear algebra package
import numpy as np

# import hdf5 to write to file
import h5py

# import matplotlib so that we can make figures
import matplotlib.pyplot as plt
from matplotlib import rcParams

# the expected error (generated by _am-mcmc.py)
file = h5py.File('am-mcmc/result.h5', 'r')

am_expectedError = file['la-mcmc/expected error'].value

# the MCMC result (generated by _la-mcmc.py)
file = h5py.File('result.h5', 'r')
la_expectedError = file['/expected error'].value

# folder to store figures
folder = 'figures'
if not os.path.isdir(folder): # make sure the folder is there
    os.mkdir(folder)

# Ntrials = file['/'].attrs['number of trials']
# expected_refinements = np.array([0.0]*len(la_expectedError))
# la_time = np.array([0.0]*len(la_expectedError))
# for i in range(Ntrials):
#     la_kappa = file['/trial '+str(i)+'/cumulative kappa refinement'].value[0]
#     la_beta = file['/trial '+str(i)+'/cumulative beta refinement'].value[0]
#     la_gamma = file['/trial '+str(i)+'/cumulative gamma refinement'].value[0]
#     expected_refinements = (float(i)*expected_refinements+la_kappa+la_beta+la_gamma)/float(i+1)
#     la_time = (float(i)*la_time+file['/trial '+str(i)+'/time'].value[0])/float(i+1)

# plot the expected error (MCMC steps)
fig = plt.figure()
rcParams['font.family'] = 'serif'
rcParams['text.usetex'] = True
ax = plt.gca()
ax.loglog(am_expectedError, color='#a65628', label='AM-MCMC')
ax.loglog(la_expectedError, color='#1f78b4', label='LA-MCMC')
ax.legend()
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.set_ylabel('Expected error', fontsize=16)
ax.set_xlabel('MCMC step', fontsize=16)
plt.savefig(folder+'/fig_expectedError_MCMCSteps.png', format='png', bbox_inches='tight')

# # plot the expected error (target evals)
# fig = plt.figure()
# rcParams['font.family'] = 'serif'
# rcParams['text.usetex'] = True
# ax = plt.gca()
# ax.loglog(am_expectedError, color='#a65628', label='AM-MCMC')
# ax.loglog(expected_refinements, la_expectedError, color='#1f78b4', label='LA-MCMC')
# ax.legend()
# ax.spines['right'].set_visible(False)
# ax.spines['top'].set_visible(False)
# ax.set_ylabel('Expected error', fontsize=16)
# ax.set_xlabel('Target density evaluations', fontsize=16)
# plt.savefig('fig_expectedError_TargetEvals.png', format='png', bbox_inches='tight')
