# python linear algebra package
import numpy as np

# import functions to help with inference
from InferenceFunctions import *

def ReadAMMCMC(filename):
    file = h5py.File('am-mcmc/results.h5', 'r')

    # read the expected error
    expectedError = file['/expected error'].value

    # read the effictive sample size for each chain
    Nproc = file['/'].attrs['number of processors']
    Ntrials = file['/'].attrs['number of trials']
    ess = [[None]*Nproc]*Ntrials
    expectedEss = 0.0
    for i in range(Ntrials):
        for j in range(Nproc):
            ess[i][j] = file['/trial '+str(i)+'/rank '+str(j)+'/effective sample size'].value.sum()/3.0
            expectedEss = UpdateExpectation(expectedEss, ess[i][j], Ntrials*i+j)

    return expectedError, ess, expectedEss

def ReadLAMCMC(filename):
    file = h5py.File(filename, 'r')

    # read the expected error
    expectedError = file['/expected error'].value

    # read the expected number of refinements
    expectedRefinements = file['/expected refinements'].value

    # read the effictive sample size for each chain
    Nproc = file['/'].attrs['number of processors']
    Ntrials = file['/'].attrs['number of trials']
    ess = [[None]*Nproc]*Ntrials
    expectedEss = 0.0
    cumRefine = [[None]*Nproc]*Ntrials
    expectedRefine = 0.0
    for i in range(Ntrials):
        for j in range(Nproc):
            ess[i][j] = file['/trial '+str(i)+'/rank '+str(j)+'/effective sample size'].value.sum()/3.0
            expectedEss = UpdateExpectation(expectedEss, ess[i][j], Ntrials*i+j)

            cumRefine[i][j] = file['/trial '+str(i)+'/rank '+str(j)].attrs['cumulative refinements']
            expectedRefine = UpdateExpectation(expectedRefine, cumRefine[i][j], Ntrials*i+j)

    return expectedError, expectedRefinements, ess, expectedEss, cumRefine, expectedRefine
